PRAPS.G.MATRIX = {
    Deck: [],
    ResultScreenTexts: {},
    Scoring: {
        currentScore: 0,
        fails: 0,
        maxPossibleScore: 1
    },
    Statistics: {
        guessedRightAway: [],
        failedRightAway: []
    },
    Round: {
        isPossibleToLearnThisRule: true,
        isPossibleToFailThisRule: true,
        ruleID: 0,
        currentPrap: null,
        currentVerb: null
    },
    currentRound: 0,
    gameIsStopped: false,
    settings: {
        // Matrix dimensions
        //linesQuantity: 6,
        linesQuantity: function() {
            if(iphone5) {
                //console.log(6);
                return 6;
            } else if(iphone4) {
                //console.log(5);
                return 5;
            } else {
                //console.log('other');
                return 6;
            }
        },
        wordsPerLine: 10,

        // Font size
        minfontSize: 36,
        maxFontSize: 70,
        fontStep: 5,

        // Margins
        minMargin: 5,
        maxMargin: 40,

        // Bottoms
        minBottom: 0,
        maxBottom: 7,

        // Lines speed
        minSpeed: 10,
        maxSpeed: 30,

        roundDuration: 20000,
        wordsPerRound: 6,
        failsTreshold: 10, // equals to amount of rounds by default times 2
        penalizeForUnfinishedGame: false,

        caseColors: {
            akk: "#efbc4d", // yellow
            dat: "#ed844f", // orange
            gen: "#61c94f", // green
            nom: "#9352c1"  // purple
        },

        linesEasing: "linear",
        wordPreviewTimeOut: 1500,

        defaultBonus: 20,
        defaultFine: -0
    },
    elements: {
        theBar: function() {
            return $('#bar');
        },
        livesHolder: function() {
            return $('.lives');
        },
        gameZone: function() {
            return $('#game-zone');
        },
        pauseButton: function() {
            return $("#pause-button");
        },
        scoreHolder: function() {
            return $("#score-holder");
        },
        resultScoreHolder: function() {
            return $("#game-over-result").find('.the-score');
        },
        maxPossibleScore: function() {
            return $("#game-over-result").find('.max-possible-score');
        },
        resultHeader: function() {
            return $("#game-over-result").find('.header');
        },
        resultMessage: function() {
            return $("#game-over-result").find('.message');
        },
        scoreFlag: function() {
            return $("#score-flag");
        },
        cardWrap: function() {
            return $("#card-wrap");
        },
        theCard: function() {
            return $("#card");
        },
        caseHolder: function() {
            return $("#case");
        },
        timeHolder: function() {
            return $("#time-holder-content");
        },
        wordHolder: function() {
            return $("#word");
        },
        translationHolder: function() {
            return $("#translation");
        },
        fillInElement: function() {
            return '<span class="fill-in-element">?</span>'
        },
        fillInHolder: function() {
            return $(".fill-in-element");
        },
        prapsList: function() {
            return $(".praps");
        },
        gameOverResult: function() {
            return $("#game-over-result");
        },
        gameOverFail: function() {
            return $("#game-over-fail");
        },
        asInEngTrue: function() {
            return $("#same-as-in-english-true");
        },
        asInEngFalse: function() {
            return $("#same-as-in-english-false");
        },
        learnErrorsButton: function() {
            return $(".praps-learn-button");
        }
    },
    effects: {
        pain: function() {
            $("#pain").css({"z-index": 999}).animate({
                "opacity": 0.85
            }, 200, function() {
                $(this).animate({
                    "opacity": 0
                }, 200, function() {
                    $(this).css({"z-index": -1});
                });
            });
        }
    },
    utilities: {
        updateScore: function(value) {
            var s = parseInt(PRAPS.G.MATRIX.elements.scoreHolder().text()) + value;
            PRAPS.G.MATRIX.elements.scoreHolder().text(s);
            PRAPS.G.MATRIX.Scoring.currentScore = s;
        },
        updateWord: function(newWord) {
            PRAPS.G.MATRIX.elements.wordHolder().text(newWord);
        },
        // Using Fisher-Yates shuffle algorithm
        shuffleArray: function(array) {
            for (var i = array.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return array;
        },
        theJudge: function(currentScore, maxPossibleScore) {
            var category = Math.ceil(currentScore / maxPossibleScore * 100 / 20);
            //var result = "";
            switch (category) {
                case 0:
                    return 'poor';
                //break;

                case 1:
                    return 'poor';
                //break;

                case 2:
                    return 'fair';
                //break;

                case 3:
                    return 'okay';
                //break;

                case 4:
                    return 'good';
                //break;

                case 5:
                    return 'fantastic';
                //break;

                default:
                    return 'fair';
            }
        },
        resetTicker: function() {
            clearInterval(PRAPS.G.MATRIX.ticker);
            PRAPS.G.MATRIX.ticker = 0;
            PRAPS.G.MATRIX.Clock.totalSeconds = PRAPS.G.MATRIX.settings.roundDuration / 1000;
        },
        resetTickerTimeout: function() {
            clearTimeout(PRAPS.G.MATRIX.tickerTimeOut);
        }
    },
    initializeTheGameInstance: function() {
        if(window.StatusBar) {StatusBar.hide();} // Hide status bar.
        //window.screen.height = 480;

        PRAPS.G.MATRIX.Deck.length = 0;
        PRAPS.U.AudioComponent.stopMusic();
        PRAPS.G.MATRIX.utilities.resetTicker();
        PRAPS.G.MATRIX.utilities.resetTickerTimeout();
        PRAPS.G.MATRIX.ResultScreenTexts = PRAPS.V.appData.texts.matrixResultScreenTexts;
        var deckForTheGame = PRAPS.V.rawRulesDeck.slice();
        var shuffledArray = PRAPS.G.MATRIX.utilities.shuffleArray(deckForTheGame);
        PRAPS.G.MATRIX.Deck = shuffledArray.slice(0, PRAPS.G.MATRIX.settings.wordsPerRound);
        PRAPS.G.MATRIX.gameIsStopped = false;
        PRAPS.G.MATRIX.currentRound = 0;
        PRAPS.G.MATRIX.Scoring.fails = 0;
        PRAPS.G.MATRIX.Scoring.currentScore = 0;
        PRAPS.G.MATRIX.Scoring.maxPossibleScore = 1;
        PRAPS.G.MATRIX.Statistics.guessedRightAway.length = 0;
        PRAPS.G.MATRIX.Statistics.failedRightAway.length = 0;
        PRAPS.G.MATRIX.prepare(PRAPS.G.MATRIX.currentRound);

    },
    prepare: function(round) {
        // Check if we reached the end of the deck
        if(!PRAPS.G.MATRIX.gameIsStopped && PRAPS.G.MATRIX.currentRound === PRAPS.G.MATRIX.Deck.length) {
            PRAPS.G.MATRIX.gameOverResult();
            // Proceed if we didn't
        } else if(!PRAPS.G.MATRIX.gameIsStopped && PRAPS.G.MATRIX.currentRound < PRAPS.G.MATRIX.Deck.length) {
            var theRoundObject = PRAPS.G.MATRIX.Deck[round];
            //PRAPS.G.MATRIX.elements.fillInHolder().removeClass("guessed").text("?"); //
            PRAPS.G.MATRIX.Round.ruleID = theRoundObject.id;
            //var theFormula = theRoundObject.formula;
            var theFormula2 = theRoundObject.formula2;
            theFormula2 = theFormula2.replace(/\[(.*?)\]/, "");
            var theTranslation = theRoundObject.translation;
            //var theExampleTranslation = theRoundObject.exampleTranslation;
            //var theExample = theRoundObject.example;
            //var theExampleWithoutPrep = theExample.replace(/\[(.*?)\]/, "___");
            var theIsTheSameInEnglish = theRoundObject.isTheSameInEnglish === 'true';
            var theCase = theRoundObject.case;
            var borderColor = PRAPS.G.MATRIX.settings.caseColors[theCase];
            var prapos = theRoundObject.prepositionsList;
            var correctPrap = theRoundObject.preposition;
            PRAPS.G.MATRIX.Round.currentPrap = theRoundObject.preposition;
            PRAPS.G.MATRIX.Round.currentVerb = theRoundObject.verb;
            //var theWord = theRoundObject.verb;
            var correctPrapsOnBoard = 0;
            var output = $('<ul class="praps"></ul>');
            for(var i = 0; i < PRAPS.G.MATRIX.settings.linesQuantity(); i++) {
                var lineQueue = '';
                var even = i % 2 === 0;
                for(var j = 0; j < PRAPS.G.MATRIX.settings.wordsPerLine; j++) {
                    var randIndex = Math.floor(Math.random() * prapos.length);
                    var isCorrect = prapos[randIndex] === correctPrap;
                    var isCorrectClass = prapos[randIndex] === correctPrap ? "correct" : "incorrect";
                    if(isCorrect) {correctPrapsOnBoard++;}
                    lineQueue += '<span data-prap-length="0" data-prap-correct="' + isCorrect +'" class="prap ' + isCorrectClass + '">' + prapos[randIndex] + '</span>';
                }
                output.append('<li data-row-length="0" data-even="'+ even +'" class="line line-' + (i + 1) +'">'+ lineQueue + '</li>');
            }

            PRAPS.G.MATRIX.Scoring.maxPossibleScore += correctPrapsOnBoard * PRAPS.G.MATRIX.settings.defaultBonus;
            PRAPS.G.MATRIX.elements.gameZone().html(output);
            //PRAPS.G.MATRIX.elements.wordHolder().text(theWord); // <-- Just the verb
            //PRAPS.G.MATRIX.elements.wordHolder().text(theExampleWithoutPrep); // <-- Example sentence
            PRAPS.G.MATRIX.elements.wordHolder().text(theFormula2).append(PRAPS.G.MATRIX.elements.fillInElement()); // <-- Special game formula
            PRAPS.G.MATRIX.elements.translationHolder().text(theTranslation); // <-- Translation

            if(theIsTheSameInEnglish) {
                PRAPS.G.MATRIX.elements.asInEngTrue().show();
                PRAPS.G.MATRIX.elements.asInEngFalse().hide();
            } else {
                PRAPS.G.MATRIX.elements.asInEngFalse().show();
                PRAPS.G.MATRIX.elements.asInEngTrue().hide();
            }

            PRAPS.G.MATRIX.elements.caseHolder().text("+" + theCase);
            PRAPS.G.MATRIX.elements.timeHolder().text(20);
            PRAPS.G.MATRIX.elements.cardWrap().css({"opacity": 1});
            PRAPS.G.MATRIX.Round.isPossibleToLearnThisRule = true;
            PRAPS.G.MATRIX.Round.isPossibleToFailThisRule = true;
            PRAPS.G.MATRIX.elements.theCard()
                .css({
                    "border-bottom-color": borderColor,
                    "opacity": 1
                });
            PRAPS.G.MATRIX.elements.cardWrap().addClass('animated bounceInDown').on('webkitAnimationEnd oanimationend animationend', function() {
                $(this).removeClass('animated bounceInDown');
            });
            PRAPS.G.MATRIX.elements.cardWrap().animate({
                opacity: 1
            }, 1000, function() {
                PRAPS.G.MATRIX.elements.theCard()
                    .delay(PRAPS.G.MATRIX.settings.wordPreviewTimeOut)
                    .animate({top: -40}, 800, function() {
                        PRAPS.G.MATRIX.elements.timeHolder().animate({opacity: 1}, 300);
                        PRAPS.G.MATRIX.fire();
                    });
            });
        }
    },
    fire: function() {
        var linesArray = $('.line');
        var prapsArray = linesArray.find('.prap');
        var gameZoneWidth = PRAPS.G.MATRIX.elements.gameZone().outerWidth();
        $.each(prapsArray, function(index, value){
            var rand = Math.random();
            var randFontSize = Math.floor(rand * (PRAPS.G.MATRIX.settings.maxFontSize - PRAPS.G.MATRIX.settings.minfontSize) / PRAPS.G.MATRIX.settings.fontStep) * PRAPS.G.MATRIX.settings.fontStep + PRAPS.G.MATRIX.settings.minfontSize;
            var randMargin = PRAPS.G.MATRIX.settings.minMargin + Math.floor(rand * (PRAPS.G.MATRIX.settings.maxMargin - PRAPS.G.MATRIX.settings.minMargin));
            var randBottom = PRAPS.G.MATRIX.settings.minBottom + Math.floor((1 - rand) * (PRAPS.G.MATRIX.settings.maxBottom - PRAPS.G.MATRIX.settings.minBottom));
            var el = $(value);
            el.css({
                "font-size": randFontSize + "px",
                "margin": "0 " + randMargin + "px 0 " + randMargin + "px",
                "bottom": randBottom + "px"
            });
            el.attr('data-prap-length', $(this).outerWidth(true));

            /* Click handlers */
            // Pause button
            PRAPS.G.MATRIX.elements.pauseButton().on('click', function() {
                //e.preventDefault();
                PRAPS.G.MATRIX.currentRound = 0;
                PRAPS.G.MATRIX.utilities.resetTicker();
                PRAPS.G.MATRIX.utilities.resetTickerTimeout();
                if(PRAPS.V.isMusic){
                    PRAPS.U.AudioComponent.playMusic();
                }
            });

            // Correct answer
            if(el.data('prap-correct')) {
                $(this).one('click', function() {
                    PRAPS.G.MATRIX.utilities.updateScore(PRAPS.G.MATRIX.settings.defaultBonus);
                    if(PRAPS.G.MATRIX.Round.isPossibleToLearnThisRule) {
                        if(window.analytics) { analytics.trackEvent('Guesses', 'Matrix Guess', PRAPS.G.MATRIX.Round.currentVerb + " + " + PRAPS.G.MATRIX.Round.currentPrap); }
                        PRAPS.G.MATRIX.Statistics.guessedRightAway.push(PRAPS.G.MATRIX.Round.ruleID);
                        PRAPS.G.MATRIX.Round.isPossibleToLearnThisRule = false;
                    }
                    var topOffset = $(this).offset().top;
                    var leftOffset = $(this).offset().left;
                    if(PRAPS.V.isSound){
                        PRAPS.U.AudioComponent.soundOK.play();
                    }

                    PRAPS.G.MATRIX.Round.isPossibleToFailThisRule = false;

                    $(this).addClass('guessed').animate({
                        top: -1 * topOffset + 70,
                        left: -1 * leftOffset + gameZoneWidth / 2
                    }, 1000, function() {
                        $(this).animate({opacity: 0}, 100);
                        PRAPS.G.MATRIX.elements.fillInHolder().addClass("guessed").text($(this).text());
                    });
                });

                //Incorrect answer
            } else {
                $(this).on('click', function() {
                    if(parseInt(PRAPS.G.MATRIX.elements.scoreHolder().text()) > 0) {
                        PRAPS.G.MATRIX.utilities.updateScore(PRAPS.G.MATRIX.settings.defaultFine);
                    }
                    if(PRAPS.G.MATRIX.Round.isPossibleToFailThisRule) {
                        if(window.analytics) { analytics.trackEvent('Fails', 'Matrix Fail', PRAPS.G.MATRIX.Round.currentVerb + " + " + PRAPS.G.MATRIX.Round.currentPrap); }
                        PRAPS.G.MATRIX.Statistics.failedRightAway.push(PRAPS.G.MATRIX.Round.ruleID);
                        PRAPS.G.MATRIX.Round.isPossibleToFailThisRule = false;
                    }
                    PRAPS.G.MATRIX.Scoring.fails++;
                    PRAPS.G.MATRIX.elements.livesHolder().find('.live-' + PRAPS.G.MATRIX.Scoring.fails).toggleClass('ion-heart ion-ios7-heart-outline');
                    PRAPS.G.MATRIX.Round.isPossibleToLearnThisRule = false;
                    $(this).addClass('failed');

                    if(PRAPS.V.isSound){
                        PRAPS.U.AudioComponent.soundFail.play();
                    }
                    PRAPS.G.MATRIX.effects.pain();

                    // Check error amount and terminate the game if needed
                    if(PRAPS.G.MATRIX.Scoring.fails >= PRAPS.G.MATRIX.settings.failsTreshold) {
                        PRAPS.G.MATRIX.gameOverFail();
                    }
                });
            }
        });

        PRAPS.G.MATRIX.Clock.start();
        $.each(linesArray, function(index, value) {
            var sumOfLengths = 0;
            var el = $(value);
            $.each(el.find('.prap'), function(idx, val) {
                sumOfLengths += parseInt($(val).data('prap-length'));
            });
            el.attr('data-row-length', sumOfLengths);

            if(el.data('even')) {
                $(this).css({
                    "right": sumOfLengths + "px"
                });
                $(this).animate({
                    right: -1 * gameZoneWidth + "px"
                }, PRAPS.G.MATRIX.settings.roundDuration, PRAPS.G.MATRIX.settings.linesEasing, function() {});
            } else {
                $(this).css({
                    "left": gameZoneWidth + "px"
                });
                $(this).animate({
                    left: -1 * sumOfLengths + "px"
                }, PRAPS.G.MATRIX.settings.roundDuration, PRAPS.G.MATRIX.settings.linesEasing, function() {});
            }
        });
        PRAPS.G.MATRIX.elements.prapsList().css({"opacity": 1});
    },
    ticker: 0,
    tickerTimeOut: 0,
    Clock: {
        totalSeconds: 20,
        start: function () {
            PRAPS.G.MATRIX.ticker = setInterval(function () {
                PRAPS.G.MATRIX.Clock.totalSeconds -= 1;
                PRAPS.G.MATRIX.elements.timeHolder().text(parseInt(PRAPS.G.MATRIX.Clock.totalSeconds % 60));
            }, 1000);

            PRAPS.G.MATRIX.tickerTimeOut = setTimeout(function() {
                PRAPS.G.MATRIX.utilities.resetTicker();
                clearInterval(PRAPS.G.MATRIX.ticker);
                PRAPS.G.MATRIX.elements.gameZone().html("");
                PRAPS.G.MATRIX.elements.cardWrap().css({"opacity": 0, "top": "-35px"});
                PRAPS.G.MATRIX.elements.theCard().css({"top": "150px"});
                PRAPS.G.MATRIX.elements.timeHolder().css({"opacity": 0});
                PRAPS.G.MATRIX.currentRound++;
                PRAPS.G.MATRIX.prepare(PRAPS.G.MATRIX.currentRound);
            }, PRAPS.G.MATRIX.settings.roundDuration);
        }
    },
    gameOverFail: function() {
        if(PRAPS.V.isSound){
            PRAPS.U.AudioComponent.soundDeath.play();
        }
        PRAPS.G.MATRIX.gameIsStopped = true;
        PRAPS.G.MATRIX.currentRound = 0;
        PRAPS.V.currentFlashCardsArray = PRAPS.G.MATRIX.Statistics.failedRightAway;
        PRAPS.G.MATRIX.elements.theBar().slideUp(400);
        PRAPS.G.MATRIX.elements.pauseButton().fadeOut(100);
        PRAPS.G.MATRIX.elements.cardWrap().fadeOut(100);
        if(PRAPS.G.MATRIX.Statistics.failedRightAway.length === 0) {PRAPS.G.MATRIX.elements.learnErrorsButton().hide();}
        PRAPS.G.MATRIX.elements.gameOverFail()
            .css({"display": "block"})
            .addClass('animated bounceInDown')
            .on('click', '.praps-ok-button', function() {
                if(window.analytics) { analytics.trackEvent('Learning', 'Matrix Learning', 'No'); }
                PRAPS.U.goToDashboard();
                if(PRAPS.V.isMusic){
                    PRAPS.U.AudioComponent.playMusic();
                }
            })
            .on('click', '.praps-learn-button', function(e) {
                e.preventDefault();
                if(window.analytics) { analytics.trackEvent('Learning', 'Matrix Learning', 'Yes', PRAPS.V.currentFlashCardsArray.length); }
                PRAPS.U.goToFlashCards();
            });

        if(window.analytics) { analytics.trackEvent('Results', 'Matrix Result', 'Killed', 0); }
    },
    gameOverResult: function() {
        if(!PRAPS.G.MATRIX.gameIsStopped) {
            if(PRAPS.V.settingsPageList[1].checked){
                PRAPS.U.AudioComponent.soundResults.play();
            }
        }
        PRAPS.V.currentFlashCardsArray = PRAPS.G.MATRIX.Statistics.failedRightAway;
        PRAPS.G.MATRIX.elements.theBar().slideUp(400);
        PRAPS.G.MATRIX.elements.pauseButton().fadeOut(100);
        PRAPS.G.MATRIX.elements.cardWrap().fadeOut(100);
        PRAPS.G.MATRIX.elements.resultHeader().text(PRAPS.G.MATRIX.ResultScreenTexts[PRAPS.G.MATRIX.utilities.theJudge(PRAPS.G.MATRIX.Scoring.currentScore, PRAPS.G.MATRIX.Scoring.maxPossibleScore)].header);
        PRAPS.G.MATRIX.elements.resultMessage().text(PRAPS.G.MATRIX.ResultScreenTexts[PRAPS.G.MATRIX.utilities.theJudge(PRAPS.G.MATRIX.Scoring.currentScore, PRAPS.G.MATRIX.Scoring.maxPossibleScore)].message);
        PRAPS.G.MATRIX.elements.resultScoreHolder().text(PRAPS.G.MATRIX.Scoring.currentScore);
        PRAPS.G.MATRIX.elements.maxPossibleScore().text(PRAPS.G.MATRIX.Scoring.maxPossibleScore - 1);
        PRAPS.U.StorageComponent.updateAppStatistics(PRAPS.G.MATRIX.Statistics);
        PRAPS.U.StorageComponent.updateScoresSums(PRAPS.G.MATRIX.Scoring.currentScore, PRAPS.G.MATRIX.Scoring.maxPossibleScore - 1);
        PRAPS.G.MATRIX.elements.gameOverResult()
            .css({"display": "block"})
            .addClass('animated bounceInDown')
            .on('click', '.praps-ok-button', function() {
                PRAPS.U.goToDashboard();
                if(PRAPS.V.isMusic){
                    PRAPS.U.AudioComponent.playMusic();
                }
            });

        if(window.analytics) { analytics.trackEvent('Results', 'Matrix Result', 'Scored', PRAPS.G.MATRIX.Scoring.currentScore / PRAPS.G.MATRIX.Scoring.maxPossibleScore - 1); }
    }
};