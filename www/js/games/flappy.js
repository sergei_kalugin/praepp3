PRAPS.G.FLAPPY = {
    
    // set up some inital values
    WIDTH: 640,
    HEIGHT:  1136,
    scale:  1,
    // the position of the canvas
    // in relation to the screen
    offset: {top: 0, left: 0},
    // store all bubble, touches, particles etc
    entities: [],

    //nextGroup: true,
    //gameOver: false,

    // for tracking player's progress
    /*
    score: {
        right: 0,
        wrong: 0
    },
    */
    // we'll set the rest of these
    // in the init function
    RATIO:  null,
    currentWidth:  null,
    currentHeight:  null,
    canvas: null,
    Images: {},
    ctx:  null,
    ua:  null,
    android: null,
    ios:  null,

    playedAlready: false,
    timer: null,

    Statistics: {
        guessedRightAway: [],
        failedRightAway: []
    },

    requestAnimationFrame: window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame,

    cancelAnimationFrame: window.cancelAnimationFrame || window.mozCancelAnimationFrame,

    elements: {
        gameOverResult: function() {
            return $("#game-over-result");
        },
        resultScoreHolder: function() {
            return $("#game-over-result").find('.the-score');
        },
        maxPossibleScore: function() {
            return $("#game-over-result").find('.max-possible-score');
        },
        resultHeader: function() {
            return $("#game-over-result").find('.header');
        },
        resultMessage: function() {
            return $("#game-over-result").find('.message');
        },
        learnButton: function() {
            return $(".praps-learn-button");
        }
    },

    init: function() {
        if(PRAPS.G.FLAPPY.timer) {
            cancelAnimationFrame(PRAPS.G.FLAPPY.timer);
        }

        // Silence please
        PRAPS.U.AudioComponent.stopMusic();

        // Hide iOS statusbar please
        if(window.StatusBar) {StatusBar.hide();}

        // Reset stats
        PRAPS.G.FLAPPY.Statistics.failedRightAway.length = 0;
        PRAPS.G.FLAPPY.Statistics.guessedRightAway.length = 0;

        // the proportion of width to height
        PRAPS.G.FLAPPY.RATIO = PRAPS.G.FLAPPY.WIDTH / PRAPS.G.FLAPPY.HEIGHT;
        // these will change when the screen is resize
        PRAPS.G.FLAPPY.currentWidth = PRAPS.G.FLAPPY.WIDTH;
        PRAPS.G.FLAPPY.currentHeight = PRAPS.G.FLAPPY.HEIGHT;

        //PRAPS.G.FLAPPY.bottom = PRAPS.G.FLAPPY.HEIGHT - 80;

        // this is our canvas element
        //PRAPS.G.FLAPPY.canvas = document.getElementsByTagName('canvas')[0];
        PRAPS.G.FLAPPY.canvas = document.getElementById("gameplay");
        // it's important to set this
        // otherwise the browser will
        // default to 320x200
        PRAPS.G.FLAPPY.canvas.width = PRAPS.G.FLAPPY.WIDTH;
        PRAPS.G.FLAPPY.canvas.height = PRAPS.G.FLAPPY.HEIGHT;
        // the canvas context allows us to
        // interact with the canvas api
        PRAPS.G.FLAPPY.ctx = PRAPS.G.FLAPPY.canvas.getContext('2d');

        // we need to sniff out android & ios
        // so we can hide the address bar in
        // our resize function
        PRAPS.G.FLAPPY.ua = navigator.userAgent.toLowerCase();
        PRAPS.G.FLAPPY.android = PRAPS.G.FLAPPY.ua.indexOf('android') > -1 ? true : false;
        PRAPS.G.FLAPPY.ios = ( PRAPS.G.FLAPPY.ua.indexOf('iphone') > -1 || PRAPS.G.FLAPPY.ua.indexOf('ipad') > -1  ) ? true : false;

        // listen for clicks
        window.addEventListener('click', function(e) {
            e.preventDefault();
            //PRAPS.G.FLAPPY.Input.set(e);
        }, false);

        window.addEventListener('mousedown', function(e) {
            e.preventDefault();
            PRAPS.G.FLAPPY.Input.set(e);
        }, false);

        // listen for touches
        window.addEventListener('touchstart', function(e) {
            e.preventDefault();
            // the event object has an array
            // called touches, we just want
            // the first touch
            PRAPS.G.FLAPPY.Input.set(e.touches[0]);
        }, false);
        window.addEventListener('touchmove', function(e) {
            // we're not interested in this
            // but prevent default behaviour
            // so the screen doesn't scroll
            // or zoom
            e.preventDefault();
        }, false);
        window.addEventListener('touchend', function(e) {
            // as above
            e.preventDefault();
        }, false);

        // we're ready to resize
        PRAPS.G.FLAPPY.resize();
        PRAPS.G.FLAPPY.reset();

        //setTimeout(PRAPS.G.FLAPPY.loop, 1000/60);
        //this.timer();
        PRAPS.G.FLAPPY.loop();
    },

    reset: function() {
        this.entities.length = 0;
        PRAPS.G.FLAPPY.Data.initialize();
        PRAPS.G.FLAPPY.Input.reset();
        PRAPS.G.FLAPPY.Tracker.reset();
        PRAPS.G.FLAPPY.Score.reset();
        PRAPS.G.FLAPPY.Bird.reset();
    },

    resize: function() {

        //PRAPS.G.FLAPPY.currentHeight = window.innerHeight;
        // resize the width in proportion
        // to the new height
        //PRAPS.G.FLAPPY.currentWidth = PRAPS.G.FLAPPY.currentHeight * PRAPS.G.FLAPPY.RATIO;

        PRAPS.G.FLAPPY.currentWidth = window.innerWidth;
        PRAPS.G.FLAPPY.currentHeight = PRAPS.G.FLAPPY.currentWidth / PRAPS.G.FLAPPY.RATIO;

        // this will create some extra space on the
        // page, allowing us to scroll pass
        // the address bar, and thus hide it.
        if (PRAPS.G.FLAPPY.android || PRAPS.G.FLAPPY.ios) {
           //document.body.style.height = (window.innerHeight + 50) + 'px';
        }

        // set the new canvas style width & height
        // note: our canvas is still 320x480 but
        // we're essentially scaling it with CSS
        PRAPS.G.FLAPPY.canvas.style.width = PRAPS.G.FLAPPY.currentWidth + 'px';
        PRAPS.G.FLAPPY.canvas.style.height = PRAPS.G.FLAPPY.currentHeight + 'px';

        // the amount by which the css resized canvas
        // is different to the actual (480x320) size.
        PRAPS.G.FLAPPY.scale = PRAPS.G.FLAPPY.currentWidth / PRAPS.G.FLAPPY.WIDTH;
        // position of canvas in relation to
        // the screen
        PRAPS.G.FLAPPY.offset.top = PRAPS.G.FLAPPY.canvas.offsetTop;
        PRAPS.G.FLAPPY.offset.left = PRAPS.G.FLAPPY.canvas.offsetLeft;

        // we use a timeout here as some mobile
        // browsers won't scroll if there is not
        // a small delay
        window.setTimeout(function() {
            window.scrollTo(0,1);
        }, 1);
    },

    // this is where all entities will be moved
    // and checked for collisions etc
    update: function() {
        
        /*
        if (PRAPS.G.FLAPPY.Tracker.shouldResetGame()) {
            PRAPS.G.FLAPPY.reset();
        }
        */

        //var restartGame = false;
        //var checkCollision = false; // we only need to check for a collision
        // if the user tapped on this game tick


        /*
        if (PRAPS.G.FLAPPY.nextGroup) {
            PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Group());
            PRAPS.G.FLAPPY.nextGroup = false;
        }
        */
        if (PRAPS.G.FLAPPY.entities.length === 0) {
            PRAPS.G.FLAPPY.Tracker.initialize();
        }



        // spawn a new instance of Touch
        // if the user has tapped the screen
        if (PRAPS.G.FLAPPY.Input.tapped) {           
            
            PRAPS.G.FLAPPY.Input.tapped = false;

            /*
            //Move bird up
            PRAPS.G.FLAPPY.Bird.move = -17;
            PRAPS.G.FLAPPY.Bird.justTapped = true;
            restartGame = true;
            */

            PRAPS.G.FLAPPY.Bird.moveUp();
            
        }

        //Update game if it's not over
        //if (!PRAPS.G.FLAPPY.gameOver) {

            // cycle through all entities and update as necessary
            for (var i = 0; i < PRAPS.G.FLAPPY.entities.length; i++) {
                //update entity
                PRAPS.G.FLAPPY.entities[i].update();

                /*
                if (PRAPS.G.FLAPPY.entities[i].type === 'group' && !PRAPS.G.FLAPPY.entities[i].done) {
                    var index;
                    var hit;
                    for (index = 0; index < PRAPS.G.FLAPPY.entities[i].bubbles.length; index++) {
                        hit = PRAPS.G.FLAPPY.collides(PRAPS.G.FLAPPY.entities[i].bubbles[index], PRAPS.G.FLAPPY.Bird);
                        if (hit && PRAPS.G.FLAPPY.entities[i].bubbles[index].correct) {
                            PRAPS.G.FLAPPY.entities[i].done = true;
                            PRAPS.G.FLAPPY.entities[i].correctChoice = true;
                            PRAPS.G.FLAPPY.score.right++;
                        } else if (hit && !PRAPS.G.FLAPPY.entities[i].bubbles[index].correct) {
                            PRAPS.G.FLAPPY.entities[i].done = true;
                            PRAPS.G.FLAPPY.entities[i].wrongChoice = true;
                            PRAPS.G.FLAPPY.score.wrong++;
                        }
                    }
                }
                */

                // delete from array if remove property
                // flag is set to true
                if (PRAPS.G.FLAPPY.entities[i].remove) {
                    PRAPS.G.FLAPPY.entities.splice(i, 1);
                }
            }

            PRAPS.G.FLAPPY.Bird.update();

        /*
        } else {
            if (restartGame) {
                PRAPS.G.FLAPPY.entities.length = 0;
                //PRAPS.G.FLAPPY.nextGroup = true;
                PRAPS.G.FLAPPY.newGroup();
                PRAPS.G.FLAPPY.gameOver = false;
                PRAPS.G.FLAPPY.score.right = 0;
                PRAPS.G.FLAPPY.score.wrong = 0;

                PRAPS.G.FLAPPY.Bird.reset();
            }
        }
        */

    },


    // this is where we draw all the entities
    render: function() {

        //PRAPS.G.FLAPPY.Draw.rect(0, 0, PRAPS.G.FLAPPY.WIDTH, PRAPS.G.FLAPPY.HEIGHT, "rgba(0,0,0,1)");
        PRAPS.G.FLAPPY.Draw.background();

        // cycle through all entities and render to canvas
        for (var i = 0; i < PRAPS.G.FLAPPY.entities.length; i += 1) {
            PRAPS.G.FLAPPY.entities[i].render();
        }

        /*
        PRAPS.G.FLAPPY.Draw.text("Richtig:", PRAPS.G.FLAPPY.WIDTH-160, PRAPS.G.FLAPPY.HEIGHT-80,
            30, 'white', 'left');
        PRAPS.G.FLAPPY.Draw.text(PRAPS.G.FLAPPY.score.right, PRAPS.G.FLAPPY.WIDTH-40, PRAPS.G.FLAPPY.HEIGHT-80,
            30, 'white', 'left');
        PRAPS.G.FLAPPY.Draw.text("Falsch:", PRAPS.G.FLAPPY.WIDTH-160, PRAPS.G.FLAPPY.HEIGHT-50,
            30, 'white', 'left');
        PRAPS.G.FLAPPY.Draw.text(PRAPS.G.FLAPPY.score.wrong, PRAPS.G.FLAPPY.WIDTH-40, PRAPS.G.FLAPPY.HEIGHT-50,
            30, 'white', 'left');
        */    

        PRAPS.G.FLAPPY.Bird.render();
        PRAPS.G.FLAPPY.Tracker.render();

        /*
        if (PRAPS.G.FLAPPY.gameOver) {
            PRAPS.G.FLAPPY.Draw.text("GAME OVER", PRAPS.G.FLAPPY.WIDTH/2, PRAPS.G.FLAPPY.HEIGHT/4+70,
                60, 'red', 'center');
            PRAPS.G.FLAPPY.Draw.text("Tap screen to restart", PRAPS.G.FLAPPY.WIDTH/2, PRAPS.G.FLAPPY.HEIGHT/4+100,
                30, 'black', 'center');
        }
        */

    },


    // the actual loop
    // requests animation frame
    // then proceeds to update
    // and render
    loop: function() {

        //requestAnimFrame( PRAPS.G.FLAPPY.loop );
        //PRAPS.G.FLAPPY.timer();

        //PRAPS.G.FLAPPY.timer = setTimeout(PRAPS.G.FLAPPY.loop, 1000/60);
        PRAPS.G.FLAPPY.timer = requestAnimationFrame(PRAPS.G.FLAPPY.loop);

        PRAPS.G.FLAPPY.update();
        PRAPS.G.FLAPPY.render();

    }
};

// return true if the rectangle and circle are colliding
// checks if two entties are touching
PRAPS.G.FLAPPY.collides = function(a, b) {

    var distance_squared = ( ((a.x - b.x) * (a.x - b.x)) +
        ((a.y - b.y) * (a.y - b.y)));

    var radii_squared = (a.r + b.r) * (a.r + b.r);

    return distance_squared < radii_squared;
};



// abstracts various canvas operations into
// standalone functions
PRAPS.G.FLAPPY.Draw = {

    clear: function() {
        PRAPS.G.FLAPPY.ctx.clearRect(0, 0, PRAPS.G.FLAPPY.WIDTH, PRAPS.G.FLAPPY.HEIGHT);
    },


    rect: function(x, y, w, h, col) {
        PRAPS.G.FLAPPY.ctx.fillStyle = col;
        PRAPS.G.FLAPPY.ctx.fillRect(x, y, w, h);
    },

    circle: function(x, y, r, col) {
        PRAPS.G.FLAPPY.ctx.fillStyle = col;
        PRAPS.G.FLAPPY.ctx.beginPath();
        PRAPS.G.FLAPPY.ctx.arc(x + 5, y + 5, r, 0,  Math.PI * 2, true);
        PRAPS.G.FLAPPY.ctx.closePath();
        PRAPS.G.FLAPPY.ctx.fill();
    },


    text: function(string, x, y, size, col, align) {
        PRAPS.G.FLAPPY.ctx.font = '700 ' + size + 'px "NotoSans"';
        PRAPS.G.FLAPPY.ctx.fillStyle = col;
        PRAPS.G.FLAPPY.ctx.textAlign=align;
        PRAPS.G.FLAPPY.ctx.fillText(string, x, y);
    },

    textLight: function(string, x, y, size, col, align) {
        PRAPS.G.FLAPPY.ctx.font = '100 ' + size + 'px "Roboto"';
        PRAPS.G.FLAPPY.ctx.fillStyle = col;
        //PRAPS.G.FLAPPY.ctx.textAlign=align;
        PRAPS.G.FLAPPY.ctx.fillText(string, x, y);
    },


    wrong: function(x, y, w, h) {
        PRAPS.G.FLAPPY.ctx.beginPath();
        PRAPS.G.FLAPPY.ctx.moveTo(x, y);
        PRAPS.G.FLAPPY.ctx.lineTo(w, h);
        PRAPS.G.FLAPPY.ctx.stroke();
    },

    background: function(){
        PRAPS.G.FLAPPY.ctx.drawImage(PRAPS.V.appImages.bgFlappy, 0, 0);
    },

    bird: function() {
        PRAPS.G.FLAPPY.ctx.drawImage(PRAPS.V.appImages.bird, PRAPS.G.FLAPPY.Bird.x, PRAPS.G.FLAPPY.Bird.y);
    },

    feather: function(x, y, v) {
        PRAPS.G.FLAPPY.ctx.drawImage(PRAPS.V.appImages["feather" + v], x, y);
    }
};



PRAPS.G.FLAPPY.Input = {

    x: 0,
    y: 0,
    tapped :false,

    set: function(data) {
        this.x = (data.pageX - PRAPS.G.FLAPPY.offset.left) / PRAPS.G.FLAPPY.scale;
        this.y = (data.pageY - PRAPS.G.FLAPPY.offset.top) / PRAPS.G.FLAPPY.scale;
        this.tapped = true;

    },

    reset: function() {
        this.x = 0;
        this.y = 0;
        this.tapped = false;
    }

};

PRAPS.G.FLAPPY.Tracker = 
{
    //playedAlready: false,
    isGameOver: false,
    calledGameOver: false,
    lives: 5,
    rules: [],
    rulesIndex: 0,
    y: PRAPS.G.FLAPPY.HEIGHT-30,
    rounds: 0,
    speed: 2,

    initialize: function() {
        this.rules = PRAPS.G.FLAPPY.Data.getNRules(10);
        this.newGroup();
    },

    newGroup: function() {
        //increase speed by 25% every five rounds
        this.rounds++;
        if (this.rounds % 5 === 0) {
            this.speed *= 1.25;
        }
        //create new group if index is less than length
        if (!this.isGameOver) {
            if (this.rulesIndex < this.rules.length) {
                PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Group(this.rules[this.rulesIndex], this.speed));
                this.rulesIndex++;    
            //otherwise get 10 new rules and restart
            } else {
                this.rules.length = 0;
                this.rules = PRAPS.G.FLAPPY.Data.getNRules(10);
                this.rulesIndex = 0;
                PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Group(this.rules[this.rulesIndex], this.speed));
                this.rulesIndex++;
            }
        }       
    },

    loseLife: function() {
        this.lives--;
        if (this.lives <= 0) {
            this.gameOver();
        }
    },


    gameOver: function() {
        this.isGameOver = true;
        
        if (!this.calledGameOver) {
            //this.playedAlready = true;
            this.calledGameOver = true;
            var result = PRAPS.G.FLAPPY.Score.getScore();
            var max = PRAPS.G.FLAPPY.Score.getMaxScore();
            var showLearnButton = PRAPS.G.FLAPPY.Statistics.failedRightAway.length;
            cancelAnimationFrame(PRAPS.G.FLAPPY.timer);

            PRAPS.G.FLAPPY.elements.resultHeader().text("Game Over");
            PRAPS.G.FLAPPY.elements.resultMessage().text("Here's how you did. Keep on learning!");
            PRAPS.G.FLAPPY.elements.resultScoreHolder().text(result);
            PRAPS.G.FLAPPY.elements.maxPossibleScore().text(max);

            PRAPS.V.currentFlashCardsArray = PRAPS.G.FLAPPY.Statistics.failedRightAway;
            PRAPS.U.StorageComponent.updateAppStatistics(PRAPS.G.FLAPPY.Statistics);
            PRAPS.U.StorageComponent.updateScoresSums(result, max);
            if(window.analytics) { analytics.trackEvent('Results', 'Flappy Result', 'Score', result / max); }

            if(showLearnButton) {PRAPS.G.FLAPPY.elements.learnButton().show();}

            PRAPS.G.FLAPPY.elements.gameOverResult()
                .css({"display": "block"})
                .addClass('animated bounceInDown')
                .on('click', '.praps-ok-button', function() {
                    //cancelAnimationFrame(PRAPS.G.FLAPPY.timer);
                    if(window.analytics) { analytics.trackEvent('Learning', 'Flappy Learning', 'No'); }
                    PRAPS.U.goToDashboard();
                    if(PRAPS.V.isMusic){
                        PRAPS.U.AudioComponent.playMusic();
                    }
                })
                .on('click', '.praps-learn-button', function(e) {
                    e.preventDefault();
                    if(window.analytics) { analytics.trackEvent('Learning', 'Flappy Learning', 'Yes', PRAPS.V.currentFlashCardsArray.length); }
                    PRAPS.U.goToFlashCards();
                });
        }
         
    },

    /*
    shouldResetGame: function() {
       return !this.calledGameOver && this.isGameOver && this.playedAlready;
    },
    */

    reset: function() {
        this.isGameOver = false;
        this.calledGameOver = false;
        this.lives = 5;
        this.rules.length = 0;
        this.rulesIndex = 0;
        this.y = PRAPS.G.FLAPPY.HEIGHT-20;
        this.rounds = 0;
        this.speed = 2;
    },

    render: function() {
        PRAPS.G.FLAPPY.Draw.text("Lives: " + this.lives, 15, this.y, 26, '#fff', 'left');
        PRAPS.G.FLAPPY.Draw.text("Score: " + PRAPS.G.FLAPPY.Score.getScore(), 15, this.y-35, 26, '#fff', 'left');
    }
};

PRAPS.G.FLAPPY.Group = function(rule, speed) {
    this.type = 'group';
    this.id = rule.id;
    this.speed = speed;
    this.x = PRAPS.G.FLAPPY.WIDTH+130;

    //get data
    //var data = PRAPS.G.FLAPPY.Data.getNRules();
    //get two preps other than the answer
    var preps = PRAPS.G.FLAPPY.Data.getPrepositions(rule);

    //create bubbles array in random order
    var tempBubbles = [
        new PRAPS.G.FLAPPY.Bubble(rule.preposition, true, 1),
        new PRAPS.G.FLAPPY.Bubble(preps[0], false, 2),
        new PRAPS.G.FLAPPY.Bubble(preps[1], false, 3)
    ];
    tempBubbles.sort(function() { return 0.5 - Math.random(); });
    tempBubbles[0].y = PRAPS.G.FLAPPY.HEIGHT/4-150;
    tempBubbles[1].y = PRAPS.G.FLAPPY.HEIGHT/2-150;
    tempBubbles[2].y = 3*PRAPS.G.FLAPPY.HEIGHT/4-150;

    tempBubbles[0].x = this.x;
    tempBubbles[1].x = this.x;
    tempBubbles[2].x = this.x;

    tempBubbles[0].color = '#E68A00';
    tempBubbles[1].color = 'blue';
    tempBubbles[2].color = 'purple';

    this.bubbles = tempBubbles;
    //this.text = rule.verb;
    //this.example = rule.exampleClean;
    this.example = rule.formula;
    //this.text = rule.example.replace(/\[[^\]]+\]/g, '___');
    this.text = rule.formula2.replace(/\[[^\]]+\]/g, '___') + ' + ' + rule.case;
    this.preposition = rule.preposition;
    this.correctChoice = false;
    this.wrongChoice = false;
    this.done = false;
    this.createdNext = false;
    this.done = false;
    this.remove = false;

    

    this.update = function() {
        this.x -= this.speed;
        if (this.x < -300 && !this.createdNext) {
            this.createdNext = true;
            PRAPS.G.FLAPPY.Tracker.newGroup();
            this.remove = true;
        }

        var hit = false;
        for (var index=0; index < this.bubbles.length; index++) {
            this.bubbles[index].update(this.x);

            //only run this if a selection has not been made yet
            if (!this.done) {
                //check for collision
                hit = PRAPS.G.FLAPPY.collides(this.bubbles[index], PRAPS.G.FLAPPY.Bird);
                if (hit && this.bubbles[index].isCorrect()) {
                    PRAPS.G.FLAPPY.Statistics.guessedRightAway.push(this.id);
                    this.correctChoice = true;
                    PRAPS.G.FLAPPY.Score.addRight();
                    this.done = true;
                    this.bubbles[index].remove = true;
                    for(var j=0; j<5; j++) {
                        PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Particle(this.bubbles[index].x, this.bubbles[index].y, 20, "rgba(129, 197, 245, 0.8)"));
                    }
                    //cancelAnimationFrame(PRAPS.G.FLAPPY.timer); // <-- Debug
                } else if (hit && !this.bubbles[index].isCorrect()) {
                    PRAPS.G.FLAPPY.Statistics.failedRightAway.push(this.id);
                    this.wrongChoice = true;
                    PRAPS.G.FLAPPY.Score.addWrong();
                    PRAPS.G.FLAPPY.Tracker.loseLife();
                    this.done = true;
                    var featherVariant = Math.floor(Math.random() * 3) + 1;
                    PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Feather(PRAPS.G.FLAPPY.Bird.x - 25, PRAPS.G.FLAPPY.Bird.y + 60, featherVariant));
                    featherVariant = Math.floor(Math.random() * 3) + 1;
                    PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Feather(PRAPS.G.FLAPPY.Bird.x + 25, PRAPS.G.FLAPPY.Bird.y - 0, featherVariant));
                    featherVariant = Math.floor(Math.random() * 3) + 1;
                    PRAPS.G.FLAPPY.entities.push(new PRAPS.G.FLAPPY.Feather(PRAPS.G.FLAPPY.Bird.x + 25, PRAPS.G.FLAPPY.Bird.y - 100, featherVariant));
                    //cancelAnimationFrame(PRAPS.G.FLAPPY.timer); // <-- Debug
                }
            }
            
        }
    };

    this.render = function() {
        for (var index=0; index < this.bubbles.length; index++) {
            if(!this.bubbles[index].remove) {
                this.bubbles[index].render();
            }
        }

        if (this.correctChoice) {
            PRAPS.G.FLAPPY.Draw.rect(0, 0, PRAPS.G.FLAPPY.WIDTH, 80, '#61c94f');
            PRAPS.G.FLAPPY.Draw.text(this.example, PRAPS.G.FLAPPY.WIDTH/2, 50, 36, 'white', 'center');
        } else if (this.wrongChoice) {
            PRAPS.G.FLAPPY.Draw.rect(0, 0, PRAPS.G.FLAPPY.WIDTH, 80, '#fc7360');
            PRAPS.G.FLAPPY.Draw.text(this.example, PRAPS.G.FLAPPY.WIDTH/2, 50, 36, 'white', 'center');
        } else {
            PRAPS.G.FLAPPY.Draw.rect(0, 0, PRAPS.G.FLAPPY.WIDTH, 80, '#222');
            PRAPS.G.FLAPPY.Draw.text(this.text, PRAPS.G.FLAPPY.WIDTH/2, 50, 36, 'white', 'center');//
        }
        
    };
};

PRAPS.G.FLAPPY.Bubble = function(text, correct, cloudNumber) {
    this.type = 'bubble';
    this.r = 20; //
    //this.speed = 2;
    this.x = PRAPS.G.FLAPPY.WIDTH + 130;
    this.y = null;
    this.text = text;
    this.color = null;
    this.correct = correct;
    this.remove = false;
    this.cloudNumber = cloudNumber;

    this.isCorrect = function() {
        return this.correct;
    };

    this.update = function(x) {
        //this.x -= this.speed;
        this.x = x;
    };

    this.render = function() {
        PRAPS.G.FLAPPY.ctx.drawImage(PRAPS.V.appImages["cloud" + this.cloudNumber], this.x, this.y);
        PRAPS.G.FLAPPY.Draw.textLight(this.text, this.x + 60, this.y + 125, 60, '#111', 'center');
    };

};

PRAPS.G.FLAPPY.Bird = {
    type: 'bird',
    r: 100,
    speed: 2,
    move: 0,
    x: PRAPS.G.FLAPPY.WIDTH / 9,
    y: 40,
    color: '#fc0',
    time: 0,
    justTapped: false,

    reset: function() {
        this.speed = 2;
        this.move = 0;
        this.y = 40;
        this.time = 0;
        this.justTapped = false;
    },

    update: function() {
        this.time += 1/90;
        this.y += 9.81*this.time + this.speed + this.move;
        //var resetTime = false;
        if (this.move < 0) {
            this.move += 1;
        }

        if (this.y+this.r+10 < 0) {
            this.y = -this.r-10;
        }

        if (this.justTapped) {
            this.time = 0;
            this.justTapped = false;
        }

        if (this.y > PRAPS.G.FLAPPY.HEIGHT+this.r) {
            //PRAPS.G.FLAPPY.gameOver = true;
            PRAPS.G.FLAPPY.Tracker.gameOver();
        }
    },

    moveUp: function() {
        this.move = -17;
        this.justTapped = true;
    },

    render: function() {
        PRAPS.G.FLAPPY.Draw.bird();
        //PRAPS.G.FLAPPY.Draw.circle(this.x, this.y, this.r, this.color); // <-- Left here for debugging
    }
};

PRAPS.G.FLAPPY.Feather = function(x, y, variant) {
    this.x = x;
    this.y = y;
    this.variant = variant;
    this.speed = Math.floor(Math.random() * 3) + 1; //
    this.remove = false;
    this.waveSize = Math.floor(Math.random() * 40);
    this.xInit = this.x;
    this.timeInit = new Date().getTime();

    this.update = function() {
        this.time = new Date().getTime() * 0.002;
        this.y += this.speed;
        //this.x += this.speed;
        this.x = this.waveSize * Math.sin(this.time) + this.xInit - (new Date().getTime() - this.timeInit) / 30;

        if (this.y > PRAPS.G.FLAPPY.HEIGHT) {
            this.remove = true;
        }
    };

    this.render = function() {
        //console.log('ouch!');
        PRAPS.G.FLAPPY.Draw.feather(this.x, this.y, this.variant);
        //PRAPS.G.FLAPPY.Draw.circle(this.x, this.y, 50, "#444");
    };
};

PRAPS.G.FLAPPY.Particle = function(x, y, r, col) {

    this.x = x;
    this.y = y;
    this.r = r;
    this.col = col;

    // determines whether particle will
    // travel to the right of left
    // 50% chance of either happening
    this.dir = (Math.random() * 2 > 1) ? 1 : -1;

    // random values so particles do no
    // travel at the same speeds
    this.vx = ~~(Math.random() * 4) * this.dir;
    this.vy = ~~(Math.random() * 7);

    this.remove = false;

    this.update = function() {

        // update coordinates
        this.x += this.vx;
        //this.y -= this.vy;
        this.y -= this.vy;

        // increase velocity so particle
        // accelerates off screen
        this.vx *= 0.99;
        this.vy *= 0.99;

        // adding this negative amount to the
        // y velocity exerts an upward pull on
        // the particle, as if drawn to the
        // surface
        this.vy -= 0.25;

        // offscreen
        if (this.y < 0) {
            this.remove = true;
        }

    };


    this.render = function() {
        PRAPS.G.FLAPPY.Draw.circle(this.x, this.y, this.r, this.col);
    };

};

PRAPS.G.FLAPPY.Score = {
    right: 0,
    wrong: 0,

    rightScore: 100,
    wrongScore: 10,

    rounds: 0,

    addRight: function() {
        this.rounds++;
        this.right++;
    },

    addWrong: function() {
        this.rounds++;
        this.wrong++;
    },

    getScore: function() {
        var score = this.right*this.rightScore-this.wrong*this.wrongScore;
        if (score < 0) {
            score = 0;
        }
        return score;
    },

    getMaxScore: function() {
        var max = this.rounds*this.rightScore;
        return max;
    },

    reset: function() {
        this.rounds = 0;
        this.right = 0;
        this.wrong = 0;
    }
};

PRAPS.G.FLAPPY.Data = {
    clonedArray: [],

    initialize: function() {
        //var tempArray = PRAPS.V.rawRulesDeck.slice();
        //this.clonedArray = tempArray;
        this.clonedArray.length = 0;
        this.clonedArray = PRAPS.V.rawRulesDeck.slice();
    },

    getNRules: function(n) {
        var rulesList = [];
        var rulesUsed = {};
        var tempRule;
        var ruleUsed;

        if (this.clonedArray.length === 0) {
            this.initialize();
        }

        for (var i = 0; i < n; i++) {
            ruleUsed = true;
            while (ruleUsed) {
                tempRule = this.clonedArray[Math.floor(Math.random() * this.clonedArray.length)];
                if (!(tempRule.id in rulesUsed)) {
                    ruleUsed = false;
                }
            }
            rulesList.push(tempRule);
            rulesUsed[tempRule.id] = true;
        }

        return rulesList;

    },

    getPrepositions: function(rule) {
        var rulePrepositionsList = rule.prepositionsList;
        var rulePreposition = rule.preposition;

        var prepsArray = [];
        var tempPrep;
        var i = 0;
        var exists = false;
        while (i < 2) {
            tempPrep = rulePrepositionsList[Math.floor(Math.random() * 
                rulePrepositionsList.length)];
            exists = false;

            if (tempPrep === rulePreposition) {
                exists = true;
            }
            for (var j = 0; j < prepsArray.length; j++) {
                if (prepsArray[j] === tempPrep) {
                    exists = true;
                }
            }

            if (!exists) {
                prepsArray.push(tempPrep);
                i++;
            }
        }
        return prepsArray;
    }
};
