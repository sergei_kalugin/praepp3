/* This object encapsulates whole app's code except ng controllers anr router.
*
* PRAPS: Präps Application
*     |
*     |-- S: global Settings
*     |-- E: non game play related Elements
*     |-- U: Utilities and components
*     |-- V: Variables, arrays and objects for global use
*     |-- G: Games object containing games engines
* */

var PRAPS = {
    S: {
        // Text
        //API_URL: "https://spreadsheets.google.com/feeds/list/1bi_9slwSQajt_BVYaN4ro9DxsiXAUZS17T7S1r28f4g/od6/public/values?alt=json-in-script&callback=praps",
        // Production
        API_URL: "https://spreadsheets.google.com/feeds/list/1webyZz_xRGhPNUD-P_ve-3wdgs-6P2Pp11rGorimKJQ/od6/public/values?alt=json-in-script&callback=praps",
        //API_URL: "text.json",
        //API_URL: "http://spooky.mmlc.northwestern.edu/~sergei/Praps/data.json?callback=?",
        defaultAjaxSuccessTimeOut: 800,

        dashboardPath: "#/praps/dashboard",
        introPath: "#/intro",
        //flashCardsPath: "#/praps/flashcards",
        flashCardsPath: "#/praps/regularflashcards",
        updatePath: "#/praps/update",

        rightAwayGuessesToLearnARule: 3 // How many times they need to guess correctly right away to 'clear' certain rule.
        //loaderIconClass: "ion-load-d"
    },

    E: {
        /* intros */
        gotItIntroButton: function() {
            return $('#got-it-intro-button');
        },
        introHolder: function() {
            return $('#praps-intro');
        },

        /* dashboard */
        dashboard: function() {
            return $('#dashboard');
        },
        dataLoadStatusBar: function() {
            return $('#data-load-status-bar');
        },

        playedCounter: function() {
            return $('#one').find('.circle');
        },

        learnedCounter: function() {
            return $('#two').find('.circle');
        },

        // Flash cards
        flashCardList: function() {
            return $('.flashcards-list');
        },

        // overall progress percent
        overallProgressPercent: function() {
            return $('#overall-progress-percent');
        }
    },

    U: {
        // App's initializer
        init: function() {
            if(window.localStorage.getItem("isInitialized") !== "yes"){
                PRAPS.U.StorageComponent.initialize();
            }
            PRAPS.U.initializeSettings();
            PRAPS.U.AudioComponent.load(50, 75, 80);
            if(PRAPS.V.isMusic){
                PRAPS.U.AudioComponent.playMusic();
            }

            PRAPS.appImagesLoader(PRAPS.V.appImagesSrc, PRAPS.U.loadAppData);
        },
        initializeSettings: function() {
            PRAPS.V.settingsPageList = PRAPS.U.StorageComponent.getSettings();
            PRAPS.V.isMusic = PRAPS.V.settingsPageList[0].checked;
            PRAPS.V.isSound = PRAPS.V.settingsPageList[1].checked;
        },
        googleSpreadSheetParser: function(spredsheet) {
            var gss = spredsheet.feed.entry;
            var rules = [];

            for(var i= 0, j=gss.length; i<j; i++) {
                var rule = {};
                rule.id = gss[i].gsx$id.$t;
                rule.verb = gss[i].gsx$verb.$t;
                rule.preposition = gss[i].gsx$preposition.$t;
                rule.case = gss[i].gsx$case.$t;
                rule.example = gss[i].gsx$example.$t;
                rule.exampleClean = gss[i].gsx$exampleclean.$t;
                rule.formula = gss[i].gsx$formula.$t;
                rule.formula2 = gss[i].gsx$formula2.$t;
                rule.prepositionsList = gss[i].gsx$prepositionslist.$t.split(",");
                rule.translation = gss[i].gsx$translation.$t;
                rule.exampleTranslation = gss[i].gsx$exampletranslation.$t;
                rule.isTheSameInEnglish = gss[i].gsx$isthesameinenglish.$t.toLowerCase();
                //rule.isTwoWay = gss[i].gsx$istwoway.$t.toLowerCase();
                //rule.isReflexive = gss[i].gsx$isreflexive.$t.toLowerCase();

                rules.push(rule);
            }

            var appVersion = gss[0].gsx$appversion.$t;
            var deckVersion = gss[0].gsx$deckversion.$t;

            //TODO: move this to google spreadsheet
            var tempAppData = {
                gameData: {
                    meta: {
                        deckVersion: deckVersion,
                        appVersion: appVersion
                    },
                    rules: rules,
                    "texts": {
                        "matrixResultScreenTexts": {
                            "poor": {
                                "header": "Hmm...",
                                "message": "Check out our flash cards and get ready for the games."
                            },
                            "fair": {
                                "header": "Well...",
                                "message": "We think you can do better. Keep learning!"
                            },
                            "okay": {
                                "header": "Just Okay",
                                "message": "Not bad at all. There\'s definitely a possibility of growing though."
                            },
                            "good": {
                                "header": "Good Job!",
                                "message": "You\'ve done pretty well but there\'s still room for progress."
                            },
                            "fantastic": {
                                "header": "Wow!",
                                "message": "Brilliant game, präpper!"
                            }
                        },
                        "AJAXLoadingTexts": {
                            "success": "you've got the latest deck, enjoy!",
                            "problem": "using default collection",
                            "inProgress": "getting the newest deck",
                            "tryToReload": "try to download the newest set"
                        }
                    }
                }
            };

            // Assign constructed array to game objects
            PRAPS.V.appData = tempAppData.gameData;
            PRAPS.V.rawRulesDeck = PRAPS.V.appData.rules; // <-- We use this later in games
            PRAPS.V.appTexts =     PRAPS.V.appData.texts;
        },
        // App's data loader
        loadAppData: function() {
            $.ajax({
                type: 'GET',
                url: PRAPS.S.API_URL,
                dataType: 'jsonp',
                contentType: 'application/json',
                jsonpCallback: 'praps',
                success: function(data){
                    //console.log('Data from API loaded');
                    //console.log(data);
                    PRAPS.U.googleSpreadSheetParser(data);

                    PRAPS.E.dataLoadStatusBar().animate({"opacity": 1}, 200).find('.icon').removeClass('ion-ios-cloud-download-outline').next().text(PRAPS.V.appTexts.AJAXLoadingTexts.inProgress);
                    PRAPS.E.dataLoadStatusBar().find('.spinner').show();
                    var initialOverallProgressPercentCalc = Math.floor(Object.size(PRAPS.U.StorageComponent.getCurrentlyLearnedObject()) / PRAPS.V.rawRulesDeck.length * 100);
                    var initialOverallProgressPercent = initialOverallProgressPercentCalc <= 100 ? initialOverallProgressPercentCalc : 100;
                    PRAPS.E.overallProgressPercent().text(initialOverallProgressPercent + "%");
                    $(".grey-2").animate({svgHeight: 205 * (1 - initialOverallProgressPercent / 100)}, 1000);

                    var initialAveragePerGameCalc = Math.floor(PRAPS.U.StorageComponent.getScoreSums().scoreSum / PRAPS.U.StorageComponent.getScoreSums().maxScoreSum * 100);
                    var initialAveragePerGame = initialAveragePerGameCalc <= 100 ? initialAveragePerGameCalc : 100;

                    $(".grey-1").animate({svgHeight: 205 * (1 - initialAveragePerGame / 100)}, 1000);

                    PRAPS.V.usingLocalDeck = false;
                    PRAPS.V.appDataLoaded = true;

                    //if(parseInt(PRAPS.U.StorageComponent.getAppVersion()) < PRAPS.V.appData.meta.appVersion) {
                    if(parseInt(PRAPS.V.appVersion) < PRAPS.V.appData.meta.appVersion) {
                        //PRAPS.V.outdated = true;
                        PRAPS.U.goToUpdate();
                    }

                    setTimeout(function() {
                        PRAPS.E.dataLoadStatusBar().toggleClass('yes').find('.icon').addClass('ion-checkmark').next().text(PRAPS.V.appTexts.AJAXLoadingTexts.success);
                        PRAPS.E.dataLoadStatusBar().find('.spinner').hide();
                    }, PRAPS.S.defaultAjaxSuccessTimeOut);
                },
                error: function() {
                    //console.log('ajax failed');
                    PRAPS.V.hasFailedToLoadAJAXData = true;
                    //console.log('Cant fetch from remote host... Try to fall back...');
                    if(PRAPS.V.rawRulesDeck.length === 0 || PRAPS.V.hasFailedToLoadAJAXData) { // Fallback to the local file
                        $.getJSON('data.json', function(localData) {
                            PRAPS.U.googleSpreadSheetParser(localData);

                            PRAPS.E.dataLoadStatusBar().animate({"opacity": 1}, 200).find('.icon').removeClass('ion-ios-cloud-download-outline').next().text(PRAPS.V.appTexts.AJAXLoadingTexts.inProgress);
                            PRAPS.E.dataLoadStatusBar().find('.spinner').show();
                            var initialOverallProgressPercentCalc = Math.floor(Object.size(PRAPS.U.StorageComponent.getCurrentlyLearnedObject()) / PRAPS.V.rawRulesDeck.length * 100);
                            var initialOverallProgressPercent = initialOverallProgressPercentCalc <= 100 ? initialOverallProgressPercentCalc : 100;
                            PRAPS.E.overallProgressPercent().text(initialOverallProgressPercent + "%");
                            $(".grey-2").animate({svgHeight: 205 * (1 - initialOverallProgressPercent / 100)}, 1000);

                            var initialAveragePerGameCalc = Math.floor(PRAPS.U.StorageComponent.getScoreSums().scoreSum / PRAPS.U.StorageComponent.getScoreSums().maxScoreSum * 100);
                            var initialAveragePerGame = initialAveragePerGameCalc <= 100 ? initialAveragePerGameCalc : 100;

                            $(".grey-1").animate({svgHeight: 205 * (1 - initialAveragePerGame / 100)}, 1000);

                            PRAPS.V.appDataLoaded = true;

                            setTimeout(function(){
                                PRAPS.E.dataLoadStatusBar().addClass('no').find('.icon').addClass('ion-alert').next().text(PRAPS.V.appTexts.AJAXLoadingTexts.problem);
                                PRAPS.E.dataLoadStatusBar().find('.spinner').hide();
                                //console.log('Using LOCAL version!');
                            }, PRAPS.S.defaultAjaxSuccessTimeOut);
                        });
                    }
                }
            });
        },
        // Audio component
        AudioComponent: {
            soundOK: null,
            soundFail: null,
            ambient: null,
            music: null,
            musicIsPlaying: false,
            load: function(okVolume, failVolume, musicVolume) {
                soundManager.setup({
                    debugMode: false,
                    onready: function() {
                        PRAPS.U.AudioComponent.soundBegin = soundManager.createSound({id: "begin", url: "sound/sound-fx/ToyChime1.mp3", volume: okVolume});
                        PRAPS.U.AudioComponent.soundOK = soundManager.createSound({id: "ok", url: "sound/sound-fx/ToyChime2.mp3", volume: okVolume});
                        PRAPS.U.AudioComponent.soundFail = soundManager.createSound({id: "fail", url: "sound/sound-fx/bump2.mp3", volume: failVolume});
                        PRAPS.U.AudioComponent.soundDeath = soundManager.createSound({id: "dead", url: "sound/sound-fx/oops.mp3", volume: failVolume});
                        PRAPS.U.AudioComponent.soundResults = soundManager.createSound({id: "results", url: "sound/sound-fx/ToyChime1.mp3", volume: failVolume});
                        PRAPS.U.AudioComponent.music = soundManager.createSound({
                            id: "music",
                            url: "sound/music/strings.mp3",
                            volume: musicVolume,
                            stream: true
                        });
                    }
                });
            },
            playMusic: function() {
                if(!PRAPS.U.AudioComponent.musicIsPlaying) {
                    PRAPS.U.AudioComponent.music.play();
                    PRAPS.U.AudioComponent.musicIsPlaying = true;
                }
            },
            stopMusic: function() {
                if(PRAPS.U.AudioComponent.musicIsPlaying) {
                    PRAPS.U.AudioComponent.music.stop();
                    PRAPS.U.AudioComponent.musicIsPlaying = false;
                }
            }
        },

        // Local Storage component
        StorageComponent: {
            GamesPlayed: {
                "matrix": 0,
                "blizzard": 0,
                "flappy": 0,
                "shooter": 0
            },
            RulesLearned: {},
            Settings: [
                {text: "Music", checked: false},
                {text: "Sounds", checked: true}
            ],
            Scores: {
                scoreSum: 0,
                maxScoreSum: 1
            },
            initialize: function() {
                localStorage.clear();
                localStorage.setItem('isInitialized', "yes");
                localStorage.setItem('gamesPlayed', JSON.stringify(PRAPS.U.StorageComponent.GamesPlayed));
                localStorage.setItem('rulesLearned', JSON.stringify(PRAPS.U.StorageComponent.RulesLearned));
                localStorage.setItem('settings', JSON.stringify(PRAPS.U.StorageComponent.Settings));
                localStorage.setItem('scores', JSON.stringify(PRAPS.U.StorageComponent.Scores));
                localStorage.setItem('appVersion', PRAPS.V.appVersion);

                // Reapply settings right away
                PRAPS.U.initializeSettings();
            },
            updateAppStatistics: function(gameStatistics) {
                if(gameStatistics.guessedRightAway.length > 0){
                    var rulesLearned = localStorage.getItem('rulesLearned');
                    rulesLearned = JSON.parse(rulesLearned);
                    for(var j=0; j < gameStatistics.guessedRightAway.length; j++){
                        if(rulesLearned.hasOwnProperty(gameStatistics.guessedRightAway[j].toString())){
                            rulesLearned[gameStatistics.guessedRightAway[j].toString()]++;
                        } else {
                            rulesLearned[gameStatistics.guessedRightAway[j].toString()] = 1;
                        }
                    }
                    localStorage.setItem('rulesLearned', JSON.stringify(rulesLearned));
                }
            },
            getPlayedSoFarObject: function() {
                return JSON.parse(localStorage.getItem('gamesPlayed'));
            },
            getCurrentlyLearnedObject: function() {
                var newObject = {};
                var rulesLearnedObject = JSON.parse(localStorage.getItem('rulesLearned'));
                for(var rule in rulesLearnedObject) {
                    if(rulesLearnedObject.hasOwnProperty(rule) && rulesLearnedObject[rule] >= PRAPS.S.rightAwayGuessesToLearnARule) {
                        newObject[rule] = rulesLearnedObject[rule];
                    }
                }
                return newObject;
            },

            updateScoresSums: function(score, maxScore) {
                var scoreObject = JSON.parse(localStorage.getItem('scores'));
                scoreObject.scoreSum += score;
                scoreObject.maxScoreSum += maxScore;
                localStorage.setItem('scores', JSON.stringify(scoreObject));
            },
            getScoreSums: function() {
                return JSON.parse(localStorage.getItem('scores'));
            },

            getSettings: function() {
                return JSON.parse(localStorage.getItem('settings'));
            },
            setSettings: function() {
                var settings = PRAPS.V.settingsPageList;
                for(var s = 0; s < settings.length; s++) {
                    delete settings[s]['$$hashKey'];
                }
                //settings[index]['checked'] = !settings[index]['checked'];
                localStorage.setItem('settings', JSON.stringify(settings));
            },

            setAppVersion: function(version) {
                localStorage.setItem("appVersion", version);
            },
            getAppVersion: function() {
                return localStorage.getItem("appVersion");
            }
        },

        // Go to functions
        goToIntro: function() {
            return window.location.replace(PRAPS.S.introPath);
        },
        goToDashboard: function() {
            return window.location.replace(PRAPS.S.dashboardPath);
        },
        goToFlashCards: function() {
            return window.location.replace(PRAPS.S.flashCardsPath);
        },
        goToUpdate: function() {
            return window.location.replace(PRAPS.S.updatePath);
        }
    },

    // Images loader. This method loads all images we need for our canvas games

    appImagesLoader: function(images, callback) {
        var appImages = PRAPS.V.appImages;

        //var images = PRAPS.G.BUBBLES.Images;
        var loadedImages = 0;
        var numImages = 0;

        for(var image in images) {
            numImages++;

            if(images.hasOwnProperty(image)) {
                appImages[image] = new Image();
                appImages[image].onload = function() {
                    if(++loadedImages >= numImages) {
                        //console.log('all images loaded!');
                        callback();
                    }
                };
                appImages[image].src = images[image];
            }
        }
    },

    V: {
        appVersion: 6, // <-- Must be updated with a new release!
        outdated: false,
        appDataLoaded: false,
        appData: {},      //
        rawRulesDeck: [], //  <--- You should use this object, it contains "rules" array.
        appTexts: {},     //  <--- You can use this object to access app's texts.
        hasFailedToLoadAJAXData: false,
        hasTriedToLoadAJAXData: false,
        usingLocalDeck: true,
        currentFlashCardsArray: [], // <--- This array stores wrong guesses ids; we can start flashcards with those ids.
        settingsPageList: [],
        isMusic: null,
        isSound: null,

        appImagesSrc: {
            bgBubbles: 'img/bubbles/bg.png',
            flake1: 'img/bubbles/flake1.png',
            flake2: 'img/bubbles/flake2.png',
            flake3: 'img/bubbles/flake3.png',
            flake4: 'img/bubbles/flake4.png',

            bgFlappy: 'img/flappy/bg@2x.png',
            bird: 'img/flappy/bird@2x.png',
            cloud1: 'img/flappy/cloud1@2x.png',
            cloud2: 'img/flappy/cloud2@2x.png',
            cloud3: 'img/flappy/cloud3@2x.png',
            feather1: 'img/flappy/feather1@2x.png',
            feather2: 'img/flappy/feather2@2x.png',
            feather3: 'img/flappy/feather3@2x.png',

            bgShooter: 'img/shooter/bg.png',
            bullet: 'img/shooter/bullet.png',
            ufo1: 'img/shooter/ufo1.png',
            ufo2: 'img/shooter/ufo2.png',
            ufo3: 'img/shooter/ufo3.png',
            ufo4: 'img/shooter/ufo4.png',
            ship: 'img/shooter/ship.png'
        },
        appImages: {}
    },

    G: {} // <-- Games object
};