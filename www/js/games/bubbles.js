PRAPS.G.BUBBLES = {

    // set up some inital values
    WIDTH: 640, 
    HEIGHT:  1136,
    GROUND: 1136-130,
    GROUNDHEIGHT: 130, 
    scale:  1,
    // the position of the canvas
    // in relation to the screen
    offset: {top: 0, left: 0},
    // store all bubble, touches, particles etc
    entities: [],

    // we'll set the rest of these
    // in the init function
    RATIO:  null,
    currentWidth:  null,
    currentHeight:  null,
    canvas: null,
    Images: {},
    ctx:  null,
    ua:  null,
    android: null,
    ios:  null,
    //gameOver: false,

    timer: null,

    requestAnimationFrame: window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame,

    cancelAnimationFrame: window.cancelAnimationFrame || window.mozCancelAnimationFrame,

    elements: {
        gameOverResult: function() {
            return $("#game-over-result");
        },
        resultScoreHolder: function() {
            return $("#game-over-result").find('.the-score');
        },
        maxPossibleScore: function() {
            return $("#game-over-result").find('.max-possible-score');
        },
        resultHeader: function() {
            return $("#game-over-result").find('.header');
        },
        resultMessage: function() {
            return $("#game-over-result").find('.message');
        },
        learnButton: function() {
            return $(".praps-learn-button");
        },
        exitButton: function() {
            return $(".exit-button");
        }
    },

    init: function() {

        // Silence please
        PRAPS.U.AudioComponent.stopMusic();

        // Hide iOS statusbar please
        if(window.StatusBar) {StatusBar.hide();}

        if(PRAPS.G.BUBBLES.timer) {
            cancelAnimationFrame(PRAPS.G.BUBBLES.timer);
        }
   
        // Silence please
        PRAPS.U.AudioComponent.stopMusic();

        // the proportion of width to height
        PRAPS.G.BUBBLES.RATIO = PRAPS.G.BUBBLES.WIDTH / PRAPS.G.BUBBLES.HEIGHT;
        // these will change when the screen is resize
        PRAPS.G.BUBBLES.currentWidth = PRAPS.G.BUBBLES.WIDTH;
        PRAPS.G.BUBBLES.currentHeight = PRAPS.G.BUBBLES.HEIGHT;
        // this is our canvas element
        //PRAPS.G.BUBBLES.canvas = document.getElementsByTagName('canvas')[0];
        PRAPS.G.BUBBLES.canvas = document.getElementById("gameplay");
        // it's important to set this
        // otherwise the browser will
        // default to 320x200
        PRAPS.G.BUBBLES.canvas.width = PRAPS.G.BUBBLES.WIDTH;
        PRAPS.G.BUBBLES.canvas.height = PRAPS.G.BUBBLES.HEIGHT;
        // the canvas context allows us to 
        // interact with the canvas api
        PRAPS.G.BUBBLES.ctx = PRAPS.G.BUBBLES.canvas.getContext('2d');
        // we need to sniff out android & ios
        // so we can hide the address bar in
        // our resize function
        PRAPS.G.BUBBLES.ua = navigator.userAgent.toLowerCase();
        PRAPS.G.BUBBLES.android = PRAPS.G.BUBBLES.ua.indexOf('android') > -1 ? true : false;
        PRAPS.G.BUBBLES.ios = ( PRAPS.G.BUBBLES.ua.indexOf('iphone') > -1 || PRAPS.G.BUBBLES.ua.indexOf('ipad') > -1  ) ? true : false;

        // set up our wave effect
        // basically, a series of overlapping circles
        // across the top of screen
        PRAPS.G.BUBBLES.wave = {
            x: -15, // x coord of first circle
            y: -5, // y coord of first circle
            r: 75, // circle radius
            time: 0, // we'll use this in calculating the sine wave
            offset: 0 // this will be the sine wave offset
        }; 
        // calculate how many circles we need to 
        // cover the screen width
        PRAPS.G.BUBBLES.wave.total = Math.ceil(PRAPS.G.BUBBLES.WIDTH / PRAPS.G.BUBBLES.wave.r) + 1;

        // listen for clicks
        window.addEventListener('click', function(e) {
            e.preventDefault();
            //PRAPS.G.BUBBLES.Input.set(e);
        }, false);

        window.addEventListener('mousedown', function(e) {
            e.preventDefault();
            PRAPS.G.BUBBLES.Input.set(e);
        }, false);

        // listen for touches
        window.addEventListener('touchstart', function(e) {
            e.preventDefault();
            // the event object has an array
            // called touches, we just want
            // the first touch
            PRAPS.G.BUBBLES.Input.set(e.touches[0]);
        }, false);
        window.addEventListener('touchmove', function(e) {
            // we're not interested in this
            // but prevent default behaviour
            // so the screen doesn't scroll
            // or zoom
            e.preventDefault();
        }, false);
        window.addEventListener('touchend', function(e) {
            // as above
            e.preventDefault();
        }, false);

        //exit button click handler
        PRAPS.G.BUBBLES.elements.exitButton().on('click', function() {
            cancelAnimationFrame(PRAPS.G.BUBBLES.timer);
            PRAPS.U.goToDashboard();
        });

        // we're ready to resize
        PRAPS.G.BUBBLES.resize();
        PRAPS.G.BUBBLES.reset();

        PRAPS.G.BUBBLES.loop();

    },

    reset: function() {
        PRAPS.G.BUBBLES.entities.length = 0;
        PRAPS.G.BUBBLES.Data.initialize();
        PRAPS.G.BUBBLES.Input.reset();
        PRAPS.G.BUBBLES.Score.reset();
        PRAPS.G.BUBBLES.Tracker.reset();
        PRAPS.G.BUBBLES.Sentence.reset();
        PRAPS.G.BUBBLES.Tracker.newSentence();
    },

    resize: function() {
    
        //PRAPS.G.BUBBLES.currentHeight = window.innerHeight;
        // resize the width in proportion
        // to the new height
        //PRAPS.G.BUBBLES.currentWidth = PRAPS.G.BUBBLES.currentHeight * PRAPS.G.BUBBLES.RATIO;

        PRAPS.G.BUBBLES.currentWidth = window.innerWidth;
        PRAPS.G.BUBBLES.currentHeight = PRAPS.G.FLAPPY.currentWidth / PRAPS.G.FLAPPY.RATIO;

        // this will create some extra space on the
        // page, allowing us to scroll pass
        // the address bar, and thus hide it.
        if (PRAPS.G.BUBBLES.android || PRAPS.G.BUBBLES.ios) {
            //document.body.style.height = (window.innerHeight + 50) + 'px';
        }

        // set the new canvas style width & height
        // note: our canvas is still 320x480 but
        // we're essentially scaling it with CSS
        PRAPS.G.BUBBLES.canvas.style.width = PRAPS.G.BUBBLES.currentWidth + 'px';
        PRAPS.G.BUBBLES.canvas.style.height = PRAPS.G.BUBBLES.currentHeight + 'px';

        // the amount by which the css resized canvas
        // is different to the actual (480x320) size.
        PRAPS.G.BUBBLES.scale = PRAPS.G.BUBBLES.currentWidth / PRAPS.G.BUBBLES.WIDTH;
        // position of canvas in relation to
        // the screen
        PRAPS.G.BUBBLES.offset.top = PRAPS.G.BUBBLES.canvas.offsetTop;
        PRAPS.G.BUBBLES.offset.left = PRAPS.G.BUBBLES.canvas.offsetLeft;

        // we use a timeout here as some mobile
        // browsers won't scroll if there is not
        // a small delay
        window.setTimeout(function() {
                window.scrollTo(0,1);
        }, 1);
    },

    // this is where all entities will be moved
    // and checked for collisions etc
    update: function() {
        // we only need to check for a collision if the user tapped on this game tick
        var checkCollision = false; 
        var hit;
        var correct;
        var color;

        PRAPS.G.BUBBLES.Tracker.update();

        // spawn a new instance of Touch
        // if the user has tapped the screen
        if (PRAPS.G.BUBBLES.Input.tapped) {
            // set tapped back to false
            // to avoid spawning a new touch
            // in the next cycle
            PRAPS.G.BUBBLES.Input.tapped = false;
            checkCollision = true;
        }

        // cycle through all entities and update as necessary
        for (var i = 0; i < PRAPS.G.BUBBLES.entities.length; i++) {
            PRAPS.G.BUBBLES.entities[i].update();

            if (PRAPS.G.BUBBLES.entities[i].type === 'bubble' && checkCollision) {
                
                if (!PRAPS.G.BUBBLES.Tracker.isGameOver()) {
                    hit = PRAPS.G.BUBBLES.collides(PRAPS.G.BUBBLES.entities[i], 
                                    {x: PRAPS.G.BUBBLES.Input.x, y: PRAPS.G.BUBBLES.Input.y, r: 7});
                }
                if (hit) {
                    //test if preposition matches sentence
                    correct = PRAPS.G.BUBBLES.Sentence.test(PRAPS.G.BUBBLES.entities[i].preposition);
                    
                    // spawn an explosion
                    for (var n = 0; n < 5; n +=1 ) {
                        if (correct) {
                            color = 'rgba(97, 201, 79,'+Math.random()*2+')';
                        } else {
                            color = 'rgba(252, 115, 96,'+Math.random()*2+')';
                        }

                        PRAPS.G.BUBBLES.entities.push(new PRAPS.G.BUBBLES.Particle(
                            PRAPS.G.BUBBLES.entities[i].x, 
                            PRAPS.G.BUBBLES.entities[i].y, 
                            5, 
                            // random opacity to spice it up a bit
                            color
                        )); 
                    }
                }
                //set remove to true if hit
                PRAPS.G.BUBBLES.entities[i].remove = hit;
            }

            // delete from array if remove property
            // flag is set to true
            if (PRAPS.G.BUBBLES.entities[i].remove) {
                PRAPS.G.BUBBLES.Tracker.bubbleRemoved();
                PRAPS.G.BUBBLES.entities.splice(i, 1);
            }
        }

        //update sentence
        PRAPS.G.BUBBLES.Sentence.update();

        // update wave offset
        // feel free to play with these values for
        // either slower or faster waves
        PRAPS.G.BUBBLES.wave.time = new Date().getTime() * 0.002;
        PRAPS.G.BUBBLES.wave.offset = Math.sin(PRAPS.G.BUBBLES.wave.time * 0.8) * 5;
    },


    // this is where we draw all the entities
    render: function() {
        //draw background
        //PRAPS.G.BUBBLES.Draw.rect(0, 0, PRAPS.G.BUBBLES.WIDTH, PRAPS.G.BUBBLES.HEIGHT, '#036');
        PRAPS.G.BUBBLES.Draw.background();

        // cycle through all entities and render to canvas
        for (var i = 0; i < PRAPS.G.BUBBLES.entities.length; i++) {
            PRAPS.G.BUBBLES.entities[i].render();
        }

        // display snazzy wave effect
        for (i = 0; i < PRAPS.G.BUBBLES.wave.total; i++) {

            PRAPS.G.BUBBLES.Draw.circle(
                        PRAPS.G.BUBBLES.wave.x + PRAPS.G.BUBBLES.wave.offset +  (i * PRAPS.G.BUBBLES.wave.r), 
                        PRAPS.G.BUBBLES.wave.y,
                        PRAPS.G.BUBBLES.wave.r, 
                        'rgba(255, 255, 255, 1)');
        }

        //PRAPS.G.BUBBLES.Draw.rect(0, PRAPS.G.BUBBLES.GROUND + 35, PRAPS.G.BUBBLES.WIDTH, PRAPS.G.BUBBLES.GROUNDHEIGHT, 'rgba(255, 255, 255, .25)');
        PRAPS.G.BUBBLES.Tracker.render();
        PRAPS.G.BUBBLES.Sentence.render();

    },


    // the actual loop
    // requests animation frame
    // then proceeds to update
    // and render
    loop: function() {
        PRAPS.G.BUBBLES.timer = requestAnimationFrame(PRAPS.G.BUBBLES.loop);
        //console.log('running');
        PRAPS.G.BUBBLES.update();
        PRAPS.G.BUBBLES.render();
    }


};

// checks if two entties are touching
PRAPS.G.BUBBLES.collides = function(a, b) {

        var distance_squared = ( ((a.x - b.x) * (a.x - b.x)) + 
                                ((a.y - b.y) * (a.y - b.y)));

        var radii_squared = (a.r + b.r) * (a.r + b.r);

        if (distance_squared < radii_squared) {
            return true;
        } else {
            return false;
        }
};


// abstracts various canvas operations into
// standalone functions
PRAPS.G.BUBBLES.Draw = {

    clear: function() {
        PRAPS.G.BUBBLES.ctx.clearRect(0, 0, PRAPS.G.BUBBLES.WIDTH, PRAPS.G.BUBBLES.HEIGHT);
    },


    rect: function(x, y, w, h, col) {
        PRAPS.G.BUBBLES.ctx.fillStyle = col;
        PRAPS.G.BUBBLES.ctx.fillRect(x, y, w, h);
    },

    circle: function(x, y, r, col) {
        PRAPS.G.BUBBLES.ctx.fillStyle = col;
        PRAPS.G.BUBBLES.ctx.beginPath();
        PRAPS.G.BUBBLES.ctx.arc(x + 5, y + 5, r, 0,  Math.PI * 2, true);
        PRAPS.G.BUBBLES.ctx.closePath();
        PRAPS.G.BUBBLES.ctx.fill();
    },


    text: function(string, x, y, size, col, align) {
        PRAPS.G.BUBBLES.ctx.font = '700 ' + size + 'px "NotoSans"';
        PRAPS.G.BUBBLES.ctx.fillStyle = col;
        PRAPS.G.BUBBLES.ctx.textAlign = align;
        PRAPS.G.BUBBLES.ctx.fillText(string, x, y);
    },

    background: function(){
        PRAPS.G.BUBBLES.ctx.drawImage(PRAPS.V.appImages.bgBubbles, 0, 0);
    },

    flake: function(x, y, v) {
        PRAPS.G.BUBBLES.ctx.drawImage(PRAPS.V.appImages["flake" + v], x, y);
    }
};



PRAPS.G.BUBBLES.Input = {

    x: 0,
    y: 0,
    tapped: false,

    set: function(data) {
        this.x = (data.pageX - PRAPS.G.BUBBLES.offset.left) / PRAPS.G.BUBBLES.scale;
        this.y = (data.pageY - PRAPS.G.BUBBLES.offset.top) / PRAPS.G.BUBBLES.scale;
        this.tapped = true;

    },

    reset: function() {
        this.x = 0;
        this.y = 0;
        this.tapped = false;
    }

};

PRAPS.G.BUBBLES.Bubble = function(preposition, idx) {

    this.type = 'bubble';
    this.r = 65;
    this.speed = (Math.random() * 3) + 1;
    

    // the amount by which the bubble
    // will move from side to side
    //this.waveSize = this.r;
    this.waveSize = 20 + (Math.random() * 100);

    var maximum = PRAPS.G.BUBBLES.WIDTH-1.5*this.waveSize;
    var minimum = 1.5*this.waveSize;
    this.x = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
    //this.x = (Math.random() * (PRAPS.G.BUBBLES.WIDTH) - this.waveSize);
    this.y = 0;
    // we need to remember the original
    // x position for our sine wave calculation
    this.xConstant = this.x;
    this.preposition = preposition;
    this.remove = false;
    this.color = '#333';
    
    this.changeColor = function(color) {
        this.color = color;
    };

    this.update = function() {

        // a sine wave is commonly a function of time
        var time = new Date().getTime() * 0.002;

        //this.y -= this.speed;
        this.y += this.speed;
        // the x coord to follow a sine wave
        this.x = this.waveSize * Math.sin(time) + this.xConstant;

        // if offscreen flag for removal
        if (this.y+this.r > PRAPS.G.BUBBLES.GROUND + 250) {
            this.remove = true;
        }

    };

    this.render = function() {
        //PRAPS.G.BUBBLES.Draw.circle(this.x, this.y, this.r, this.color);
        PRAPS.G.BUBBLES.Draw.flake(this.x-70, this.y-70, idx);
        PRAPS.G.BUBBLES.Draw.text(this.preposition, this.x+8, this.y+15, 30, '#333', 'center');
    };

};



PRAPS.G.BUBBLES.Particle = function(x, y, r, col) {

    this.x = x;
    this.y = y;
    this.r = r;
    this.col = col;

    // determines whether particle will
    // travel to the right of left
    // 50% chance of either happening
    this.dir = (Math.random() * 2 > 1) ? 1 : -1;

    // random values so particles do no
    // travel at the same speeds
    this.vx = ~~(Math.random() * 4) * this.dir;
    this.vy = ~~(Math.random() * 7);

    this.remove = false;

    this.update = function() {

        // update coordinates
        this.x += this.vx;
        //this.y -= this.vy;
        this.y -= this.vy;

        // increase velocity so particle
        // accelerates off screen
        this.vx *= 0.99;
        this.vy *= 0.99;

        // adding this negative amount to the
        // y velocity exerts an upward pull on
        // the particle, as if drawn to the
        // surface
        this.vy -= 0.25;

        // offscreen
        if (this.y < 0) {
            this.remove = true;
        }

    };


    this.render = function() {
        PRAPS.G.BUBBLES.Draw.circle(this.x, this.y, this.r, this.col);
    };

};

PRAPS.G.BUBBLES.Sentence = {
    type: 'sentence',

    x: PRAPS.G.BUBBLES.WIDTH/2,
    y: 30,

    sentence: "",
    preposition: null,

    sentencePrev: "",
    xPrev: null,
    isSliding: false,
    
    change: function(s, p) {
        this.sentencePrev = this.sentence;
        this.xPrev = this.x;
        this.isSliding = true;
        this.x = 1.5*PRAPS.G.BUBBLES.WIDTH;

        /*
        if (this.sentence !== "") {
            this.sentencePrev = this.sentence;
            this.xPrev = this.x;
            this.isSliding = true; 
            this.x = 1.5*PRAPS.G.BUBBLES.WIDTH;
        }
        */

        this.sentence = s;
        this.preposition = p;
    },

    slide: function() {
        this.x -= 10;
        this.xPrev -= 10;
        if (this.x === PRAPS.G.BUBBLES.WIDTH/2) {
            this.isSliding = false;
            this.sentencePrev = "";
        }
    },

    test: function(preposition) {
        if (this.preposition === preposition) {
            PRAPS.G.BUBBLES.Tracker.correctSelection(preposition);
            return true;
        } else {
            PRAPS.G.BUBBLES.Tracker.wrongSelection(preposition);
            return false;
        }
    },

    update: function() {
        if (this.isSliding) {
            this.slide();
        }
    },

    render: function() {
        //draw ground
        PRAPS.G.BUBBLES.Draw.text(this.sentence, this.x, this.y, 24, 'black', 'center');
        PRAPS.G.BUBBLES.Draw.text(this.sentencePrev, this.xPrev, this.y, 24, 'black', 'center');
    },

    reset: function() {
        this.x = PRAPS.G.BUBBLES.WIDTH/2;
        this.y = 45;
        this.sentence = "";
        this.preposition = null;
        this.sentencePrev = "";
        this.xPrev = null;
        this.isSliding = false;
    }

};

PRAPS.G.BUBBLES.Tracker = {
    gameOver: false,
    calledGameOver: false,
    numSentences: 10,
    ruleId: null,
    example: null,
    preposition: null,
    prepositionsList: [],
    prepositionsIndex: 0,
    rules: [],
    rulesIndex: 0,
    selectionsLeft: 0,
    nextBubble: 40,
    bubbleCounter: 0,
    correctList: [],
    wrongList: [],
    y: PRAPS.G.BUBBLES.HEIGHT-30,

    newSentence: function() {
        if (this.rulesIndex < this.rules.length) {
            var tempRule = this.rules[this.rulesIndex];
        
            this.ruleId = tempRule.id;
            this.example = tempRule.example.replace(/\[[^\]]+\]/g, '___');
            this.preposition = tempRule.preposition;
            
            var tempPrepositionsList = tempRule.prepositionsList.slice();
            //randomize order of prepositions
            for (var i = tempPrepositionsList.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = tempPrepositionsList[i];
                tempPrepositionsList[i] = tempPrepositionsList[j];
                tempPrepositionsList[j] = temp;
            }
            this.prepositionsList = tempPrepositionsList;
            

            this.rulesIndex++;
            //this.sentencesLeft--;
            this.prepositionsIndex = 0;
            this.selectionsLeft = 5;

            PRAPS.G.BUBBLES.Sentence.change(this.example, this.preposition);  
        } else {
            //PRAPS.G.BUBBLES.gameOver = true;
            this.renderGameOver();
        }
    },

    correctSelection: function() {
        //if 0 or 1 miestakes are made add to correctList
        if (this.selectionsLeft >= 4) {
            this.correctList.push(this.ruleId);
        }
        PRAPS.G.BUBBLES.Score.addRight();
        this.newSentence();
    },

    wrongSelection: function() {
        this.selectionsLeft--;
        PRAPS.G.BUBBLES.Score.addWrong();
        if (this.selectionsLeft <= 0) {
            this.wrongList.push(this.ruleId);
            this.newSentence();
        }
    },

    bubbleRemoved: function() {
        this.bubbleCounter--;
    },

    isGameOver: function() {
        return this.gameOver;
    },

    update: function() {
        
        /*
        if (this.rules.length === 0) {
            this.rules = PRAPS.G.BUBBLES.DATA.getNRules(this.numSentences);
        }
        */

        this.nextBubble--;
        // if the counter is less than zero
        if (this.nextBubble < 0) {
            //check that bubble index is less than length of array
            if (this.prepositionsIndex < this.prepositionsList.length) {
                // put a new instance of bubble into our entities array
                if (!this.gameOver) {
                    var idx = Math.ceil(Math.random() * 4);

                    PRAPS.G.BUBBLES.entities.push(new PRAPS.G.BUBBLES.Bubble(
                        this.prepositionsList[this.prepositionsIndex], idx));
                }
                // reset the counter with a random value
                this.nextBubble = ( Math.random() * 40 ) + 40;  
                this.prepositionsIndex++;
                this.bubbleCounter++;
             } 
            

            //check if all bubbles have been removed
            if (this.bubbleCounter <= 0) {
                //if run out of prepositions, then create a new sentence
                if (this.ruleId !== null) {
                    this.wrongList.push(this.ruleId);
                }
                this.newSentence();
            }
        }
            
    },

    render: function() {
        PRAPS.G.BUBBLES.Draw.text("Sentences left: "+(this.numSentences - this.rulesIndex + 1), 10, this.y + 15, 26, 'black', 'left');
        //PRAPS.G.BUBBLES.Draw.text("Selections left: "+this.selectionsLeft, PRAPS.G.BUBBLES.WIDTH/2+50, this.y-25, 26, 'black', 'left');
        PRAPS.G.BUBBLES.Draw.text(""+this.selectionsLeft, PRAPS.G.BUBBLES.WIDTH-55, this.y+10, 70, 'black', 'left');
        PRAPS.G.BUBBLES.Draw.text("Score: "+PRAPS.G.BUBBLES.Score.getScore(), 10, this.y-25, 26, 'rgba(0,0,0,1)', 'left');
    },

    renderGameOver: function() {
        this.gameOver = true;

        if (!this.calledGameOver) {
            this.calledGameOver = true;

            PRAPS.G.BUBBLES.elements.resultHeader().text("Game Over");
            PRAPS.G.BUBBLES.elements.resultMessage().text("Here's how you did. Keep on learning!");
            PRAPS.G.BUBBLES.elements.resultScoreHolder().text(PRAPS.G.BUBBLES.Score.getScore());
            PRAPS.G.BUBBLES.elements.maxPossibleScore().text(PRAPS.G.BUBBLES.Score.maxScore);
            PRAPS.G.BUBBLES.elements.exitButton().hide();
            PRAPS.U.StorageComponent.updateAppStatistics(
                {guessedRightAway: PRAPS.G.BUBBLES.Tracker.correctList, 
                    failedRightAway: PRAPS.G.BUBBLES.Tracker.wrongList});
            PRAPS.U.StorageComponent.updateScoresSums(
                PRAPS.G.BUBBLES.Score.getScore(), PRAPS.G.BUBBLES.Score.maxScore-1);

            cancelAnimationFrame(PRAPS.G.BUBBLES.timer);
            if(window.analytics) { analytics.trackEvent('Results', 'Bubbles Result', 'Score', PRAPS.G.BUBBLES.Score.getScore() / PRAPS.G.BUBBLES.Score.maxScore); }

            PRAPS.G.BUBBLES.elements.gameOverResult()
                .css({"display": "block"})
                .addClass('animated bounceInDown')
                .on('click', '.praps-ok-button', function() {
                    //cancelAnimationFrame(PRAPS.G.BUBBLES.timer);
                    PRAPS.U.goToDashboard();
                    if(PRAPS.V.isMusic){
                        PRAPS.U.AudioComponent.playMusic();
                    }
                    if(window.analytics) { analytics.trackEvent('Learning', 'Bubbles Learning', 'No'); }
                })
                .on('click', '.praps-learn-button', function(e) {
                    e.preventDefault();
                    if(window.analytics) { analytics.trackEvent('Learning', 'Bubbles Learning', 'Yes', PRAPS.V.currentFlashCardsArray.length); }
                    PRAPS.U.goToFlashCards();
                });
        } 
    },

    reset: function() {
        this.gameOver = false;
        this.calledGameOver = false;
        this.numSentences = 10;
        this.ruleId = null;
        this.example = null;
        this.preposition = null;
        this.prepositionsList.length = 0;
        this.prepositionsIndex = 0;
        this.rules.length = 0;
        this.rulesIndex = 0;
        this.selectionsLeft = 0;
        this.nextBubble = 40;
        this.bubbleCounter = 0;
        this.correctList.length = 0;
        this.wrongList.length = 0;
        this.y = PRAPS.G.BUBBLES.HEIGHT-30;

        this.rules = PRAPS.G.BUBBLES.Data.getNRules(this.numSentences);
    }


};

PRAPS.G.BUBBLES.Score = {
    right: 0,
    wrong: 0,

    rightScore: 100,
    wrongScore: 10,

    maxScore: 1000,

    addRight: function() {
        this.right++;
    },

    addWrong: function() {
        this.wrong++;
    },

    getScore: function() {
        var score = this.right*this.rightScore-this.wrong*this.wrongScore;
        if (score < 0) {
            score = 0;
        }
        return score;
    },

    reset: function() {
        this.right = 0;
        this.wrong = 0;
    }
};

PRAPS.G.BUBBLES.Data = 
{
    clonedArray: [],

    initialize: function() {
        this.clonedArray.length = 0;
        this.clonedArray = PRAPS.V.rawRulesDeck.slice();
    },

    getNRules: function(n) {
        var rulesList = [];
        var rulesUsed = {};
        var tempRule;
        var ruleUsed;

        if (this.clonedArray.length === 0) {
            this.initialize();
        }

        for (var i = 0; i < n; i++) {
            ruleUsed = true;
            while (ruleUsed) {
                tempRule = this.clonedArray[Math.floor(Math.random() * this.clonedArray.length)];
                if (!(tempRule.id in rulesUsed)) {
                    ruleUsed = false;
                }
            }
            rulesList.push(tempRule);
            rulesUsed[tempRule.id] = true;
        }

        return rulesList;

    }
};