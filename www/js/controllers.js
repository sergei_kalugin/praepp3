//angular.module('starter.controllers', ['ionic'])
angular.module('starter.controllers', [])

    .controller('AppCtrl', function($scope) {
        $scope.$root.menuEnabled = true;
    })
    .controller('IntroCtrl', function($scope) {
        if(window.StatusBar) {StatusBar.show();} //
        $scope.slideChanged = function(index) {
            //return console.log("slide changed! " + index);
            var buttons = $(".pager-buttons");
            buttons.find('.pager-button').removeClass('active');
            buttons.find('.pager-button:eq(' + index + ')').addClass('active');
        };

        $scope.buttonClicked = function() {
            localStorage.setItem('needIntro', 'no');
            if(window.analytics) { analytics.trackEvent('App', 'Watched Intro'); }
        };

        var initAnalytics = function() {
            if(window.analytics) { analytics.trackView('Intro'); }
        };
        initAnalytics();
    })
    .controller('DashboardCtrl', function($scope, $ionicNavBarDelegate) {
        //if(window.StatusBar) {StatusBar.hide();}

        $ionicNavBarDelegate.showBackButton(false);

        if(localStorage.getItem('needIntro') !== "no") {
            PRAPS.U.goToIntro();
        }

        if(PRAPS.V.hasTriedToLoadAJAXData && PRAPS.V.hasFailedToLoadAJAXData) {
            PRAPS.E.dataLoadStatusBar().animate({"opacity": 1}, 50).on('click', function() {
                PRAPS.E.dataLoadStatusBar().removeClass('no');
                PRAPS.U.loadAppData();
            });
        }

        /* Calculating dashboard numbers */

        // Games played:
        var gamesPlayed = 0;
        var Games = localStorage.getItem('gamesPlayed');
        Games = JSON.parse(Games);
        for(var game in Games) {
            if(Games.hasOwnProperty(game)){
                gamesPlayed += Games[game];
            }
        }
        $scope.gamesPlayed = gamesPlayed;
        $scope.averagePerGame = Math.floor(PRAPS.U.StorageComponent.getScoreSums().scoreSum / PRAPS.U.StorageComponent.getScoreSums().maxScoreSum * 100);

        // Rules learned:
        $scope.rulesLearned = Object.size(PRAPS.U.StorageComponent.getCurrentlyLearnedObject());
        if(PRAPS.V.rawRulesDeck.length) {
            $scope.progressPercentageText = Math.floor($scope.rulesLearned / PRAPS.V.rawRulesDeck.length * 100) <= 100 ? Math.floor($scope.rulesLearned / PRAPS.V.rawRulesDeck.length * 100) : 100;
        } else {
            $scope.progressPercentageText = 0;
        }

        // Animate and adjust fonts
        if(PRAPS.V.appDataLoaded) {
            $(".grey-1").animate({svgHeight: 205 * (1 - $scope.averagePerGame / 100)}, 1000);
            $(".grey-2").animate({svgHeight: 205 * (1 - $scope.progressPercentageText / 100)}, 1000);
        }
        if($scope.gamesPlayed > 99) {
            PRAPS.E.playedCounter().css({"font-size": "2.1em"})
        }

        if($scope.rulesLearned > 99) {
            PRAPS.E.learnedCounter().css({"font-size": "2.1em"})
        }

        /* Click handler to increase amount of played games */
        $scope.gamesPlayedPlusOne = function(game) {
            Games[game]++;
            localStorage.setItem('gamesPlayed', JSON.stringify(Games));
        };

        // Reset "failed right away" array so we can play with all flashcards from the menu
        PRAPS.V.currentFlashCardsArray.length = 0;

        PRAPS.E.dashboard().fadeIn(100);

        var init = function() {
            if(window.StatusBar) {StatusBar.show();}
            if(window.analytics) { analytics.trackView('Dashboard'); }
        };
        init();
    })
    .controller('CreditsCtrl', function($scope) {
        $scope.goToMMLC = function() {
            //window.open(encodeURI("http://mmlc.northwestern.edu/#praepp=external"), "_blank");
            if(window.device) {
                if(device.platform.toUpperCase() === 'IOS') {
                    window.open(encodeURI("http://mmlc.northwestern.edu/#praepp=external"), "_blank");
                } else {
                    window.open("http://mmlc.northwestern.edu", "_system");
                }
            }
        };

        var platform = "";
        if(window.device) {
            platform = " (" + device.platform +")";
        }

        $scope.versionNumber = PRAPS.V.appVersion + platform;

        //$scope.goToNU = function() {
        //    window.open(encodeURI("http://northwestern.edu/#praepp=external"), "_blank");
        //};

        var init = function() {
            if(window.analytics) { analytics.trackView('Credits'); }
        };
        init();
    })
    .controller('UpdateCtrl', function($scope) {
        $scope.$root.menuEnabled = false;
        $scope.$on('$stateChangeStart', function() {
            $scope.$root.menuEnabled = true;
        });

        $scope.go = function() {
            //window.open(encodeURI("http://mmlc.northwestern.edu/praepp#praepp=external"), "_blank");
            if(window.device) {
                if(device.platform.toUpperCase() === 'IOS') {
                    window.open(encodeURI("http://mmlc.northwestern.edu/praepp#praepp=external"), "_blank");
                } else {
                    window.open("http://mmlc.northwestern.edu", "_system");
                }
            }
        };

        var init = function() {
            if(window.analytics) { analytics.trackView('Update'); }
        };
        init();
    })
    .controller('SettingsCtrl', function($scope, $ionicModal) {
        $scope.resetLocalData = function() {
            $scope.modal.hide();
            $scope.modal.remove();
            PRAPS.U.AudioComponent.stopMusic();
            if(window.analytics) {analytics.trackEvent('App', 'Local Storage Reset')}
            PRAPS.U.StorageComponent.initialize();
            PRAPS.U.goToDashboard();
        };
        //PRAPS.V.settingsPageList = PRAPS.U.StorageComponent.getSettings();
        $scope.settingsList = PRAPS.V.settingsPageList;
        //$scope.settingsList = PRAPS.U.StorageComponent.getSettings();
        //$scope.settingsList = [];
        $scope.changedToggle = function(index) {
            //console.log('Changing item #' + index);
            PRAPS.U.StorageComponent.setSettings();
            if(index === 0) {
                if(!PRAPS.V.settingsPageList[0].checked){
                    PRAPS.U.AudioComponent.stopMusic();
                }
                if(PRAPS.V.settingsPageList[0].checked){
                    PRAPS.U.AudioComponent.playMusic();
                }
            } else if(index === 1) {
                if(PRAPS.V.settingsPageList[1].checked){
                    PRAPS.U.AudioComponent.soundBegin.play();
                }
            }
        };

        //$scope.isMusic = PRAPS.V.settingsPageList[0].checked;
        //$scope.isSound = PRAPS.V.settingsPageList[1].checked;

        $ionicModal.fromTemplateUrl('templates/modals/settings.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        var init = function() {
            if(window.analytics) { analytics.trackView('Settings'); }
        };
        init();
    })
    .controller('SettingsModalCtrl', function($scope) {})
    .controller('PlayedCtrl', function($scope) {
        $scope.gamesPlayed = PRAPS.U.StorageComponent.getPlayedSoFarObject();

        var count = 0;
        for(var g in $scope.gamesPlayed) {
            if($scope.gamesPlayed.hasOwnProperty(g)) {
                count += $scope.gamesPlayed[g];
            }
        }
        $scope.total = count;

        $scope.goToDashboard = function() {
            PRAPS.U.goToDashboard();
        };

        var init = function() {
            if(window.analytics) { analytics.trackView('Played'); }
        };
        init();
    })
    .controller('LearnedCtrl', function($scope) {
        var deck = PRAPS.V.rawRulesDeck;
        var keysArray = Object.keys(PRAPS.U.StorageComponent.getCurrentlyLearnedObject());
        $scope.rulesLearned = $.grep(deck, function(object) {
           return $.inArray(object.id.toString(), keysArray) !== -1;
        });

        $scope.goToDashboard = function() {
            PRAPS.U.goToDashboard();
        };

        var init = function() {
            if(window.analytics) { analytics.trackView('Learned'); }
        };
        init();
    })
    .controller('LearnCtrl', function($scope) {
        $scope.goToDashboard = function() {
            PRAPS.U.goToDashboard();
        };

        var init = function() {
            if(window.analytics) { analytics.trackView('Learn'); }
        };
        init();
    })
    .controller('FlashcardsCtrl', function($scope) {
        var init = function() {
            if(window.analytics) { analytics.trackView('Flash Cards'); }
        };
        init();
    })
    .controller('CasesFlashcardsCtrl', function($scope) {
        var init = function() {
            if(window.analytics) { analytics.trackView('Cases Flash Cards'); }
        };
        init();
    })
    .controller('RegularFlashcardsModalCtrl', function($scope) {})
    .controller('RegularFlashcardsCtrl', function($scope, $ionicModal, $filter) {
        if(window.StatusBar) {StatusBar.show();}

        var list = $('#cards-container'),
            controls =$('.controls'),
            deck = PRAPS.V.rawRulesDeck,
            keysArray = PRAPS.V.currentFlashCardsArray,
            rulesFailed = $.grep(deck, function(object) {
                return $.inArray(object.id, keysArray) !== -1;
            });

        $scope.isLearningErrors = rulesFailed.length !== 0;
        $scope.flashcards = PRAPS.V.currentFlashCardsArray.length === 0 ? PRAPS.V.rawRulesDeck : rulesFailed;
        //$scope.flashcards.filter = '';
        $scope.hideBackButton = true;

        //console.log($scope.flashcards);

        $ionicModal.fromTemplateUrl('templates/modals/regular-flashcards.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.filters = [
            {type: 'preposition', text: 'an',   checked: false, t: 'an'},
            {type: 'preposition', text: 'auf',  checked: false, t: 'auf'},
            {type: 'preposition', text: 'aus',  checked: false, t: 'aus'},
            {type: 'preposition', text: 'für',  checked: false, t: 'für'},
            {type: 'preposition', text: 'in',   checked: false, t: 'in'},
            {type: 'preposition', text: 'mit',  checked: false, t: 'mit'},
            {type: 'preposition', text: 'nach', checked: false, t: 'nach'},
            {type: 'preposition', text: 'über', checked: false, t: 'über'},
            {type: 'preposition', text: 'um',   checked: false, t: 'um'},
            {type: 'preposition', text: 'von',  checked: false, t: 'von'},
            {type: 'preposition', text: 'vor',  checked: false, t: 'vor'},
            {type: 'preposition', text: 'zu',   checked: false, t: 'zu'},
            {type: 'case',        text: 'akk',   checked: false, t: 'akk'},
            {type: 'case',        text: 'dat',   checked: false, t: 'dat'},
            {type: 'isTheSameInEnglish', text: 'yes',   checked: false, t: 'eng'},
            {type: 'isTheSameInEnglish', text: 'no',   checked: false, t: 'ger'}
        ];

        $scope.filter = {
            prepositions: ['an', 'auf','aus', 'für', 'in', 'mit', 'nach', 'über', 'um', 'von', 'vor', 'zu'],
            cases: ['akk', 'dat'],
            isTheSameInEnglish: ['yes', 'no']
        };

        $scope.isActive = function(el) {
            switch(el.type) {
                case 'preposition':
                    return ($scope.filter.prepositions.indexOf(el.text)) >=0;
                    break;

                case 'case':
                    return ($scope.filter.cases.indexOf(el.text)) >=0;
                    break;

                case 'isTheSameInEnglish':
                    return ($scope.filter.isTheSameInEnglish.indexOf(el.text)) >=0;
                    break;
            }
        };

        $scope.applyFilter = function(filtersModel) {

            $scope.filter = {
                prepositions: [],
                cases: [],
                isTheSameInEnglish: []
            };

            for(var i= 0, length = filtersModel.length; i<length;i++) {
                var filter = filtersModel[i];
                switch (filter.type) {
                    case 'preposition':
                        if(filter.checked) {
                            $scope.filter.prepositions.push(filter.text);
                        } else {
                            var index = $scope.filter.prepositions.indexOf(filter.text);
                            if(index>=0) $scope.filter.prepositions.splice(index, 1);
                        }
                        break;

                    case 'case':
                        if(filter.checked) {
                            $scope.filter.cases.push(filter.text);
                        } else {
                            var index = $scope.filter.cases.indexOf(filter.text);
                            if(index>=0) $scope.filter.cases.splice(index, 1);
                        }
                        break;

                    case 'isTheSameInEnglish':
                        if(filter.checked) {
                            $scope.filter.isTheSameInEnglish.push(filter.text);
                        } else {
                            var index = $scope.filter.isTheSameInEnglish.indexOf(filter.text);
                            if(index>=0) $scope.filter.isTheSameInEnglish.splice(index, 1);
                        }
                        break;
                }
            }

            //console.log($scope.filter);
            //console.log($scope.filters);
        };

        $scope.flashCardsFilter = function(card) {
            //return card.preposition === $scope.filter.preposition ||
            //        card.case === $scope.filter.case ||
            //        card.isTheSameInEnglish === $scope.filter.isTheSameInEnglish;

            if(typeof $scope.filter.prepositions === 'undefined') {
                return true;
            } else {
                var isEng = card.isTheSameInEnglish === 'true' ? 'yes' : 'no';
                return ($scope.filter.prepositions.indexOf(card.preposition) >= 0) ||
                    ($scope.filter.cases.indexOf(card.case) >= 0) ||
                    ($scope.filter.isTheSameInEnglish.indexOf(isEng) >= 0);
            }
        };

        if($scope.isLearningErrors) {
            $('.learn-your-lessons').show();
        } else {
            controls.show();
        }

        list.on('click', '.collection-repeat-container > div', function() {
            var formula = $(this).find('.formula').text();
            if(window.analytics) { analytics.trackEvent('Flash Cards', 'Flip', formula); }
            $(this).toggleClass('flip');
        });

        controls.on('click', '.control', function() {
            $(this).addClass('active').siblings().removeClass('active');
        });

        var init = function() {
            if(window.analytics) { analytics.trackView('Regular Flash Cards'); }
        };
        init();
    })

    .controller('GamesCtrl', function($scope, $controller) {
        $controller('DashboardCtrl', {$scope: $scope});

        var init = function() {
            if(window.analytics) { analytics.trackView('Pick A Game'); }
        };
        init();
    })
    .controller('BubblesCtrl', function($scope) {
        var init = function() {
            if(window.analytics) { analytics.trackView('Bubbles'); }
        };
        init();
    })
    .controller('ShooterCtrl', function($scope) {
        var init = function() {
            if(window.analytics) { analytics.trackView('Shooter'); }
        };
        init();
    })
    .controller('FlappyCtrl', function($scope) {
        var init = function() {
            if(window.analytics) { analytics.trackView('Flappy'); }
        };
        init();
    })
    .controller('MatrixCtrl', function($scope) {
        $scope.roundDuration = 20;
        $scope.deckLength = PRAPS.G.MATRIX.settings.failsTreshold;
        $scope.getNumber = function(num) {
            return new Array(num);
        };
        if(PRAPS.V.settingsPageList[1].checked) {
            PRAPS.U.AudioComponent.soundBegin.play();
        }
        var init = function() {
            if(window.analytics) { analytics.trackView('Matrix'); }
        };
        init();
    });


