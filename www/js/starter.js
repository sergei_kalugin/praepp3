ionic.Platform.ready(function(){
    try{
        angular.bootstrap(document.getElementsByTagName('body')[0], ['starter']);
        //console.log('Angular has been bootstrapped successfully!');

        //$ionicPlatform.ready(function() {
            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                //console.log('-- Keyboard ok...');
            }
            if(window.StatusBar) {
                StatusBar.show();
                //console.log('-- Statusbar ok...');
            }
            if(window.analytics) {
                analytics.startTrackerWithId('UA-51493570-1');
                analytics.trackEvent("App", "App started");
                //console.log('-- Google analytics ok...');
            }

        // Let's go!
        PRAPS.U.init();
        //});
    }
    catch(e){
        //console.log('Something went wrong...');
        //console.log('ERROR: ' + e);
    }
});

angular.module('starter', ['ionic', 'starter.controllers'])

    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('matrix', {
                url: "/matrix",
                templateUrl: "templates/games/matrix.html",
                controller: "MatrixCtrl"
            })

            .state('bubbles', {
                url: "/bubbles",
                templateUrl: "templates/games/bubbles.html",
                controller: "BubblesCtrl"
            })

            .state('flappy', {
                url: "/flappy",
                templateUrl: "templates/games/flappy.html",
                controller: "FlappyCtrl"
            })

            .state('shooter', {
                url: "/shooter",
                templateUrl: "templates/games/shooter.html",
                controller: "ShooterCtrl"
            })

            .state('intro', {
                url: "/intro",
                templateUrl: "templates/intro.html",
                controller: "IntroCtrl"
            })

            .state('praps', {
                url: "/praps",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppCtrl'
            })

            .state('praps.howtoplay', {
                url: "/howtoplay",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/howtoplay.html"
                    }
                }
            })

            .state('praps.learn', {
                url: "/learn",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/learn.html",
                        controller: "LearnCtrl"
                    }
                }
            })

            .state('praps.dashboard', {
                url: "/dashboard",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/dashboard.html",
                        controller: 'DashboardCtrl'
                    }
                }
            })

            .state('praps.credits', {
                url: "/credits",
                views: {
                    'menuContent': {
                        templateUrl: "templates/credits.html",
                        controller: 'CreditsCtrl'
                    }
                }
            })

            .state('praps.update', {
                url: "/update",
                views: {
                    'menuContent': {
                        templateUrl: "templates/update.html",
                        controller: 'UpdateCtrl'
                    }
                }
            })

            .state('praps.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        templateUrl: "templates/settings.html",
                        controller: 'SettingsCtrl'
                    }
                }
            })

            .state('praps.played', {
                url: "/played",
                views: {
                    'menuContent': {
                        templateUrl: "templates/played.html",
                        controller: 'PlayedCtrl'
                    }
                }
            })

            .state('praps.learned', {
                url: "/learned",
                views: {
                    'menuContent': {
                        templateUrl: "templates/learned.html",
                        controller: 'LearnedCtrl'
                    }
                }
            })

            .state('praps.games', {
                url: "/games",
                views: {
                    'menuContent': {
                        templateUrl: "templates/games.html",
                        controller: 'GamesCtrl'
                    }
                }
            })

            .state('praps.flashcards', {
                url: "/flashcards",
                views: {
                    'menuContent': {
                        templateUrl: "templates/flashcards.html",
                        controller: 'FlashcardsCtrl'
                    }
                }
            })

            .state('praps.regularflashcards', {
                url: "/regularflashcards",
                //templateUrl: "templates/flashcards/regular.html",
                //controller: "FlashcardsRegularCtrl"
                views: {
                    'menuContent': {
                        templateUrl: "templates/flashcards/regular.html",
                        controller: 'RegularFlashcardsCtrl'
                    }
                }
            })

            .state('praps.casesflashcards', {
                url: "/casesflashcards",
                //templateUrl: "templates/flashcards/cases.html",
                //controller: "FlashcardsCasesCtrl",
                views: {
                    'menuContent': {
                        templateUrl: "templates/flashcards/cases.html",
                        controller: 'CasesFlashcardsCtrl'
                    }
                }
            });

        // if none of the above states are matched:
        $urlRouterProvider.otherwise('/praps/dashboard');
    })

    // Getting rid of any text next to "Back" button
    .config(function($ionicConfigProvider) {
        $ionicConfigProvider.backButton.text('').previousTitleText(false);
        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.swipeBackEnabled(false);
    });
