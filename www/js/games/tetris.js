PRAPS.G.TETRIS = {

    // set up some inital values
    WIDTH: 640,
    HEIGHT:  1136,
    scale:  1,
    // the position of the canvas
    // in relation to the screen
    offset: {top: 0, left: 0},
    // store all bubble, touches, particles etc
    entities: [],
    // the amount of game ticks until
    // we spawn a bubble
    nextBubble: 100,
    nextBlock: 100,
    bottom: null,
    nextGroup: true,
    choiceRects: null,
    // for tracking player's progress
    score: {
        taps: 0,
        hit: 0,
        escaped: 0,
        accuracy: 0
    },
    // we'll set the rest of these
    // in the init function
    RATIO:  null,
    currentWidth:  null,
    currentHeight:  null,
    canvas: {},
    ctx:  null,
    ua:  null,
    android: null,
    ios:  null,
    DATA: {
        "gamedata": {
            "grammarrules": {
                "rule": [
                    {
                        "ruletext": "sich anpassen an + Akk.",
                        "case": "accusative",
                        "preposition": "an",
                        "verb": "anpassen"
                    },
                    {
                        "ruletext": "sich ärgern über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "ägern",
                        "example": "Mein Mitbewohner ärgert sich über meine laute Musik"
                    },
                    {
                        "ruletext": "sich bedanken bei + Dat.",
                        "case": "dative",
                        "preposition": "bei",
                        "verb": "bedanken"
                    },
                    {
                        "ruletext": "sich bedanken für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "bedanken"
                    },
                    {
                        "ruletext": "sich beschweren bei + Dat.",
                        "case": "dative",
                        "preposition": "bei",
                        "verb": "beschweren"
                    },
                    {
                        "ruletext": "sich beschweren über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "beschweren"
                    },
                    {
                        "ruletext": "sich bewerben um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "bewerben",
                        "example": "Die Studentin bewirbt sich um ein Fulbright-Stipendium."
                    },
                    {
                        "ruletext": "bitten um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "bitten",
                        "example": "Er bittet die Ärztin um schnelle Hilfe."
                    },
                    {
                        "ruletext": "danken für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "danken"
                    },
                    {
                        "ruletext": "danken an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "danken",
                        "example": "Er denkt den ganzen Tag an Schokoladenkuchen."
                    },
                    {
                        "ruletext": "sich entschuldigen bei + Dat.",
                        "case": "dative",
                        "preposition": "bei",
                        "verb": "entschuldigen"
                    },
                    {
                        "ruletext": "sich entschuldigen für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "entschuldigen"
                    },
                    {
                        "ruletext": "(sich) erinnern an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "erinnern",
                        "example": "Sie erinnert sich an ihre Kindheit in der Schweiz."
                    },
                    {
                        "ruletext": "sich erkundigen bei + Dat.",
                        "case": "dative",
                        "preposition": "bei",
                        "verb": "erkundigen"
                    },
                    {
                        "ruletext": "sich erkundigen nach + Dat.",
                        "case": "dative",
                        "preposition": "nach",
                        "verb": "erkundigen"
                    },
                    {
                        "ruletext": "sich entscheiden für",
                        "case": "all",
                        "preposition": "für",
                        "verb": "entscheiden"
                    },
                    {
                        "ruletext": "fragen nach + Dat.",
                        "case": "dative",
                        "preposition": "nach",
                        "verb": "fragen"
                    },
                    {
                        "ruletext": "sich freuen auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "freuen",
                        "example": "Sie freut sich schon im August auf Weihnachten."
                    },
                    {
                        "ruletext": "sich fürchten vor + Dat.",
                        "case": "dative",
                        "preposition": "vor",
                        "verb": "fürchten",
                        "example": "Er fürchtet sich sehr vor Clowns."
                    },
                    {
                        "ruletext": "gehören zu + Dat.",
                        "case": "dative",
                        "preposition": "zu",
                        "verb": "gehören"
                    },
                    {
                        "ruletext": "gehen um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "gehen"
                    },
                    {
                        "ruletext": "(sich) gewöhnen an + Akk.",
                        "case": "accusative",
                        "preposition": "an",
                        "verb": "gewöhnen"
                    },
                    {
                        "ruletext": "glauben an + Akk.",
                        "case": "accusative",
                        "preposition": "an",
                        "verb": "glauben"
                    },
                    {
                        "ruletext": "sich handeln um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "handeln"
                    },
                    {
                        "ruletext": "helfen bei + Dat.",
                        "case": "dative",
                        "preposition": "bei",
                        "verb": "helfen"
                    },
                    {
                        "ruletext": "herrschen über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "herrschen"
                    },
                    {
                        "ruletext": "hoffen auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "hoffen",
                        "example": " "
                    },
                    {
                        "ruletext": "sich informieren über",
                        "case": "all",
                        "preposition": "über",
                        "verb": "informieren",
                        "example": " "
                    },
                    {
                        "ruletext": "sich interessieren für",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "interessieren",
                        "example": "Interessieren Sie sich für Politik?"
                    },
                    {
                        "ruletext": "sich irren in + Dat.",
                        "case": "dative",
                        "preposition": "in",
                        "verb": "irren",
                        "example": " "
                    },
                    {
                        "ruletext": "kämpfen mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "kämpfen",
                        "example": " "
                    },
                    {
                        "ruletext": "kämpfen gegen + Akk.",
                        "case": "accusative",
                        "preposition": "gegen",
                        "verb": "kämpfen",
                        "example": " "
                    },
                    {
                        "ruletext": "kämpfen für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "kämpfen",
                        "example": " "
                    },
                    {
                        "ruletext": "sich konzentrieren auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "konzentrieren",
                        "example": "Die Pilotin konzentriert sich auf die Landung."
                    },
                    {
                        "ruletext": "sich kümmern um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "kümmern",
                        "example": "Der Sohn kümmert sich um seine Eltern."
                    },
                    {
                        "ruletext": "lachen über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "lachen"
                    },
                    {
                        "ruletext": "leiden an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "leiden"
                    },
                    {
                        "ruletext": "leiden unter + Dat.",
                        "case": "dative",
                        "preposition": "unter",
                        "verb": "leiden"
                    },
                    {
                        "ruletext": "nachdenken über + Akk",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "nachdenken"
                    },
                    {
                        "ruletext": "protestieren gegen + Akk.",
                        "case": "accusative",
                        "preposition": "gegen",
                        "verb": "protestieren"
                    },
                    {
                        "ruletext": "sich rächen an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "rächen"
                    },
                    {
                        "ruletext": "sich rächen für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "rächen"
                    },
                    {
                        "ruletext": "rechnen auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "rechnen"
                    },
                    {
                        "ruletext": "rechnen mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "rechnen"
                    },
                    {
                        "ruletext": "reden mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "reden"
                    },
                    {
                        "ruletext": "reden über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "reden"
                    },
                    {
                        "ruletext": "riechen nach + Akk.",
                        "case": "accusative",
                        "preposition": "nach",
                        "verb": "reichen",
                        "example": "Sie riecht nach teurem Parfum."
                    },
                    {
                        "ruletext": "schreiben an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "schreiben"
                    },
                    {
                        "ruletext": "schreiben an + Akk.",
                        "case": "accusative",
                        "preposition": "an",
                        "verb": "schreiben"
                    },
                    {
                        "ruletext": "schreiben über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "schreiben"
                    },
                    {
                        "ruletext": "(sich) schützen vor + Dat.",
                        "case": "dative",
                        "preposition": "vor",
                        "verb": "schützen"
                    },
                    {
                        "ruletext": "sich sehnen nach + Dat.",
                        "case": "dative",
                        "preposition": "nach",
                        "verb": "sehnen"
                    },
                    {
                        "ruletext": "(sich) sorgen um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "sorgen"
                    },
                    {
                        "ruletext": "(sich) sorgen für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "sorgen"
                    },
                    {
                        "ruletext": "sprechen mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "sprechen"
                    },
                    {
                        "ruletext": "sprechen über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "sprechen"
                    },
                    {
                        "ruletext": "sprechen von + Dat.",
                        "case": "dative",
                        "preposition": "von",
                        "verb": "sprechen"
                    },
                    {
                        "ruletext": "staunen über + Akk",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "staunen"
                    },
                    {
                        "ruletext": "sterben an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "sterben"
                    },
                    {
                        "ruletext": "sterben für + Akk.",
                        "case": "accusative",
                        "preposition": "für",
                        "verb": "sterben"
                    },
                    {
                        "ruletext": "sich streiten mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "streiten"
                    },
                    {
                        "ruletext": "sich streiten um + Akk.",
                        "case": "accusative",
                        "preposition": "um",
                        "verb": "streiten"
                    },
                    {
                        "ruletext": "teilnehmen an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "teilnehmen"
                    },
                    {
                        "ruletext": "träumen von + Dat.",
                        "case": "dative",
                        "preposition": "von",
                        "verb": "träumen",
                        "example": "Im Januar träumt sie von einer Reise nach Mexiko."
                    },
                    {
                        "ruletext": "zu tun haben mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "haben"
                    },
                    {
                        "ruletext": "sich unterhalten mit + Dat.",
                        "case": "dative",
                        "preposition": "mit",
                        "verb": "unterhalten"
                    },
                    {
                        "ruletext": "sich unterhalten über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "unterhalten"
                    },
                    {
                        "ruletext": "sich verlassen auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "verlassen"
                    },
                    {
                        "ruletext": "sich verlieben in + Akk.",
                        "case": "accusative",
                        "preposition": "in",
                        "verb": "verlieben",
                        "example": "Sie verliebt sich immer in die falsche Person."
                    },
                    {
                        "ruletext": "vertrauen auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "vertrauen"
                    },
                    {
                        "ruletext": "verzichten auf + Akk",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "verzichten"
                    },
                    {
                        "ruletext": "(sich) vorbereiten auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "vorbereiten"
                    },
                    {
                        "ruletext": "warnen vor + Dat.",
                        "case": "dative",
                        "preposition": "vor",
                        "verb": "warnen"
                    },
                    {
                        "ruletext": "warten auf + Akk.",
                        "case": "accusative",
                        "preposition": "auf",
                        "verb": "warten",
                        "example": "Ich warte nicht mehr auf Godot."
                    },
                    {
                        "ruletext": "sich wundern über + Akk.",
                        "case": "accusative",
                        "preposition": "über",
                        "verb": "wundern"
                    },
                    {
                        "ruletext": "zweifeln an + Dat.",
                        "case": "dative",
                        "preposition": "an",
                        "verb": "zweifeln"
                    }
                ]
            }
        },

        prepositions: [
            "an", "über", "bei", "für", "um", "nach", "auf", "vor", "zu", "in",
            "mit", "gegen", "unter", "von"
        ],

        getData: function() {
            var tempRule = this.gamedata.grammarrules.rule[Math.floor(Math.random() *
                this.gamedata.grammarrules.rule.length)];
            return {verb: tempRule.verb, preposition: tempRule.preposition};
        },

        getPrepositions: function(preposition) {
            var prepsArray = [];
            var tempPrep;
            var i = 0;
            var exists = false;
            while (i < 3) {
                tempPrep = this.prepositions[Math.floor(Math.random() * this.prepositions.length)];
                exists = false;

                if (tempPrep === preposition) {
                    exists = true;
                }
                for (var j = 0; j < prepsArray.length; j++) {
                    if (prepsArray[j] === tempPrep) {
                        exists = true;
                    }
                }

                if (!exists) {
                    prepsArray.push(tempPrep);
                    i++;
                }
            }
            return prepsArray;
        }
    },

init: function() {

        // the proportion of width to height
        PRAPS.G.TETRIS.RATIO = PRAPS.G.TETRIS.WIDTH / PRAPS.G.TETRIS.HEIGHT;
        // these will change when the screen is resize
        PRAPS.G.TETRIS.currentWidth = PRAPS.G.TETRIS.WIDTH;
        PRAPS.G.TETRIS.currentHeight = PRAPS.G.TETRIS.HEIGHT;
        PRAPS.G.TETRIS.bottom = PRAPS.G.TETRIS.HEIGHT - 80;
        // this is our canvas element
        PRAPS.G.TETRIS.canvas = document.getElementsByTagName('canvas')[0];
        // it's important to set this
        // otherwise the browser will
        // default to 320x200
        PRAPS.G.TETRIS.canvas.width = PRAPS.G.TETRIS.WIDTH;
        PRAPS.G.TETRIS.canvas.height = PRAPS.G.TETRIS.HEIGHT;
        // the canvas context allows us to
        // interact with the canvas api
        PRAPS.G.TETRIS.ctx = PRAPS.G.TETRIS.canvas.getContext('2d');
        // we need to sniff out android & ios
        // so we can hide the address bar in
        // our resize function
        PRAPS.G.TETRIS.ua = navigator.userAgent.toLowerCase();
        PRAPS.G.TETRIS.android = PRAPS.G.TETRIS.ua.indexOf('android') > -1 ? true : false;
        PRAPS.G.TETRIS.ios = ( PRAPS.G.TETRIS.ua.indexOf('iphone') > -1 || PRAPS.G.TETRIS.ua.indexOf('ipad') > -1  ) ? true : false;

        // listen for clicks
        window.addEventListener('click', function(e) {
            e.preventDefault();
            PRAPS.G.TETRIS.Input.set(e);
        }, false);

        // listen for touches
        window.addEventListener('touchstart', function(e) {
            e.preventDefault();
            // the event object has an array
            // called touches, we just want
            // the first touch
            PRAPS.G.TETRIS.Input.set(e.touches[0]);
        }, false);
        window.addEventListener('touchmove', function(e) {
            // we're not interested in this
            // but prevent default behaviour
            // so the screen doesn't scroll
            // or zoom
            e.preventDefault();
        }, false);
        window.addEventListener('touchend', function(e) {
            // as above
            e.preventDefault();
        }, false);

        // we're ready to resize
        PRAPS.G.TETRIS.resize();

        PRAPS.G.TETRIS.loop();

    },


    resize: function() {

        PRAPS.G.TETRIS.currentHeight = window.innerHeight;
        // resize the width in proportion
        // to the new height
        PRAPS.G.TETRIS.currentWidth = PRAPS.G.TETRIS.currentHeight * PRAPS.G.TETRIS.RATIO;

        // this will create some extra space on the
        // page, allowing us to scroll pass
        // the address bar, and thus hide it.
        if (PRAPS.G.TETRIS.android || PRAPS.G.TETRIS.ios) {
            document.body.style.height = (window.innerHeight + 50) + 'px';
        }

        // set the new canvas style width & height
        // note: our canvas is still 320x480 but
        // we're essentially scaling it with CSS
        PRAPS.G.TETRIS.canvas.style.width = PRAPS.G.TETRIS.currentWidth + 'px';
        PRAPS.G.TETRIS.canvas.style.height = PRAPS.G.TETRIS.currentHeight + 'px';

        // the amount by which the css resized canvas
        // is different to the actual (480x320) size.
        PRAPS.G.TETRIS.scale = PRAPS.G.TETRIS.currentWidth / PRAPS.G.TETRIS.WIDTH;
        // position of canvas in relation to
        // the screen
        PRAPS.G.TETRIS.offset.top = PRAPS.G.TETRIS.canvas.offsetTop;
        PRAPS.G.TETRIS.offset.left = PRAPS.G.TETRIS.canvas.offsetLeft;


        //Create choice rectangles
        PRAPS.G.TETRIS.choiceRects = [
            new PRAPS.G.TETRIS.ChoiceRect('red', 0),
            new PRAPS.G.TETRIS.ChoiceRect('green', PRAPS.G.TETRIS.WIDTH/4),
            new PRAPS.G.TETRIS.ChoiceRect('blue', PRAPS.G.TETRIS.WIDTH/2),
            new PRAPS.G.TETRIS.ChoiceRect('orange', 3*PRAPS.G.TETRIS.WIDTH/4)
        ];

        // we use a timeout here as some mobile
        // browsers won't scroll if there is not
        // a small delay
        window.setTimeout(function() {
            window.scrollTo(0,1);
        }, 1);
    },

    // this is where all entities will be moved
    // and checked for collisions etc
    update: function() {
        var i,
            checkCollision = false; // we only need to check for a collision
        // if the user tapped on this game tick


        if (PRAPS.G.TETRIS.nextGroup) {
            PRAPS.G.TETRIS.entities.push(new PRAPS.G.TETRIS.Group());
            PRAPS.G.TETRIS.nextGroup = false;
        }

        // spawn a new instance of Touch
        // if the user has tapped the screen
        if (PRAPS.G.TETRIS.Input.tapped) {
            // keep track of taps; needed to
            // calculate accuracy
            PRAPS.G.TETRIS.score.taps += 1;
            // add a new touch
            PRAPS.G.TETRIS.entities.push(new PRAPS.G.TETRIS.Touch(PRAPS.G.TETRIS.Input.x, PRAPS.G.TETRIS.Input.y));
            // set tapped back to false
            // to avoid spawning a new touch
            // in the next cycle
            PRAPS.G.TETRIS.Input.tapped = false;
            checkCollision = true;
        }

        // cycle through all entities and update as necessary
        for (i = 0; i < PRAPS.G.TETRIS.entities.length; i += 1) {
            PRAPS.G.TETRIS.entities[i].update();

            if (PRAPS.G.TETRIS.entities[i].type === 'group' && !PRAPS.G.TETRIS.entities[i].done
                && checkCollision) {
                //Loop over choices and check for a collision
                //Check if answer is right
                var index;
                var hit;
                for (index=0; index < PRAPS.G.TETRIS.entities[i].choices.length; index++) {
                    hit = PRAPS.G.TETRIS.collides(PRAPS.G.TETRIS.entities[i].choices[index],
                        {x: PRAPS.G.TETRIS.Input.x, y: PRAPS.G.TETRIS.Input.y, r: 7});
                    //Correct answer
                    if (hit && PRAPS.G.TETRIS.entities[i].choices[index].correct) {
                        PRAPS.G.TETRIS.entities[i].correctChoice = true;
                        PRAPS.G.TETRIS.entities[i].done = true;
                        PRAPS.G.TETRIS.score.hit += 1;
                        PRAPS.G.TETRIS.nextGroup = true;
                        //PRAPS.G.TETRIS.entities[i].choices.length = 0;
                        //Wrong answer
                    } else if (hit && !PRAPS.G.TETRIS.entities[i].choices[index].correct){
                        PRAPS.G.TETRIS.entities[i].correctChoice = false;
                        PRAPS.G.TETRIS.entities[i].wrongChoice = true;
                        PRAPS.G.TETRIS.entities[i].selectedChoice = index;
                        PRAPS.G.TETRIS.entities[i].done = true;
                        //PRAPS.G.TETRIS.entities[i].displayChoices = false;
                        //PRAPS.G.TETRIS.entities[i].choices.length = 0;
                    }
                }

                PRAPS.G.TETRIS.entities[i].remove = PRAPS.G.TETRIS.entities[i].correctChoice;


                //Block hits bottom
            } else if (PRAPS.G.TETRIS.entities[i].type === 'group' && PRAPS.G.TETRIS.entities[i].justStopped) {
                PRAPS.G.TETRIS.nextGroup = true;
                PRAPS.G.TETRIS.entities[i].justStopped = false;
                PRAPS.G.TETRIS.entities[i].done = true;
                PRAPS.G.TETRIS.entities[i].displayChoices = false;
                //PRAPS.G.TETRIS.entities[i].choices.length = 0;
            }

            // delete from array if remove property
            // flag is set to true
            if (PRAPS.G.TETRIS.entities[i].remove) {
                PRAPS.G.TETRIS.entities.splice(i, 1);
            }
        }

        // calculate accuracy
        PRAPS.G.TETRIS.score.accuracy = (PRAPS.G.TETRIS.score.hit / PRAPS.G.TETRIS.score.taps) * 100;
        PRAPS.G.TETRIS.score.accuracy = isNaN(PRAPS.G.TETRIS.score.accuracy) ?
            0 :
            ~~(PRAPS.G.TETRIS.score.accuracy); // a handy way to round floats

    },


    // this is where we draw all the entities
    render: function() {

        var i;


        PRAPS.G.TETRIS.Draw.rect(0, 0, PRAPS.G.TETRIS.WIDTH, PRAPS.G.TETRIS.HEIGHT, '#6699FF');

        for (i=0; i < PRAPS.G.TETRIS.choiceRects.length; i++) {
            PRAPS.G.TETRIS.choiceRects[i].render();
        }

        // cycle through all entities and render to canvas
        for (i = 0; i < PRAPS.G.TETRIS.entities.length; i += 1) {
            PRAPS.G.TETRIS.entities[i].render();
        }

    },


    // the actual loop
    // requests animation frame
    // then proceeds to update
    // and render
    loop: function() {

        requestAnimFrame( PRAPS.G.TETRIS.loop );

        if (PRAPS.G.TETRIS.bottom > 5) {
            PRAPS.G.TETRIS.update();
        }
        PRAPS.G.TETRIS.render();
    }


}

// return true if the rectangle and circle are colliding
PRAPS.G.TETRIS.collides = function (rect, circle){
    var distX = Math.abs(circle.x - rect.x-rect.width/2);
    var distY = Math.abs(circle.y - rect.y-rect.height/2);

    if (distX > (rect.width/2 + circle.r)) { return false; }
    if (distY > (rect.height/2 + circle.r)) { return false; }

    if (distX <= (rect.width/2)) { return true; }
    if (distY <= (rect.height/2)) { return true; }

    var dx=distX-rect.width/2;
    var dy=distY-rect.height/2;
    return (dx*dx+dy*dy<=(circle.r*circle.r));
};



// abstracts various canvas operations into
// standalone functions
PRAPS.G.TETRIS.Draw = {

    clear: function() {
        PRAPS.G.TETRIS.ctx.clearRect(0, 0, PRAPS.G.TETRIS.WIDTH, PRAPS.G.TETRIS.HEIGHT);
    },


    rect: function(x, y, w, h, col) {
        PRAPS.G.TETRIS.ctx.fillStyle = col;
        PRAPS.G.TETRIS.ctx.fillRect(x, y, w, h);
    },

    circle: function(x, y, r, col) {
        PRAPS.G.TETRIS.ctx.fillStyle = col;
        PRAPS.G.TETRIS.ctx.beginPath();
        PRAPS.G.TETRIS.ctx.arc(x + 5, y + 5, r, 0,  Math.PI * 2, true);
        PRAPS.G.TETRIS.ctx.closePath();
        PRAPS.G.TETRIS.ctx.fill();
    },


    text: function(string, x, y, size, col) {
        PRAPS.G.TETRIS.ctx.font = 'bold '+size+'px Arial';
        PRAPS.G.TETRIS.ctx.fillStyle = col;
        PRAPS.G.TETRIS.ctx.textAlign="center";
        PRAPS.G.TETRIS.ctx.fillText(string, x, y);
    },

    wrong: function(x, y, w, h) {
        PRAPS.G.TETRIS.ctx.beginPath();
        PRAPS.G.TETRIS.ctx.moveTo(x, y);
        PRAPS.G.TETRIS.ctx.lineTo(w, h);
        PRAPS.G.TETRIS.ctx.stroke();
    }

};



PRAPS.G.TETRIS.Input = {

    x: 0,
    y: 0,
    tapped :false,

    set: function(data) {
        this.x = (data.pageX - PRAPS.G.TETRIS.offset.left) / PRAPS.G.TETRIS.scale;
        this.y = (data.pageY - PRAPS.G.TETRIS.offset.top) / PRAPS.G.TETRIS.scale;
        this.tapped = true;

    }

};

PRAPS.G.TETRIS.Touch = function(x, y) {

    this.type = 'touch';    // we'll need this later
    this.x = x;             // the x coordinate
    this.y = y;             // the y coordinate
    this.r = 5;             // the radius
    this.opacity = 1;       // inital opacity. the dot will fade out
    this.fade = 0.05;       // amount by which to fade on each game tick
    // this.remove = false;    // flag for removing this entity. PRAPS.G.TETRIS.update
    // will take care of this

    this.update = function() {
        // reduct the opacity accordingly
        this.opacity -= this.fade;
        // if opacity if 0 or less, flag for removal
        this.remove = (this.opacity < 0) ? true : false;
    };

    this.render = function() {
        PRAPS.G.TETRIS.Draw.circle(this.x, this.y, this.r, 'rgba(255,0,0,'+this.opacity+')');
    };

};

PRAPS.G.TETRIS.Group = function() {
    this.type = 'group';

    //get game data
    var data = PRAPS.G.TETRIS.DATA.getData();
    //get 3 other prepositions
    var preps = PRAPS.G.TETRIS.DATA.getPrepositions(data.preposition);
    var tempChoices = [
        new PRAPS.G.TETRIS.Choice(data.preposition, true),
        new PRAPS.G.TETRIS.Choice(preps[0], false),
        new PRAPS.G.TETRIS.Choice(preps[1], false),
        new PRAPS.G.TETRIS.Choice(preps[2], false)
    ];
    tempChoices.sort(function() { return 0.5 - Math.random() });
    tempChoices[0].x = 0;
    tempChoices[1].x = PRAPS.G.TETRIS.WIDTH/4;
    tempChoices[2].x = PRAPS.G.TETRIS.WIDTH/2;
    tempChoices[3].x = 3*PRAPS.G.TETRIS.WIDTH/4;

    this.choices = tempChoices;
    this.correctChoice = false;
    this.wrongChoice = false;
    this.selectedChoice;
    this.done = false;
    this.block = new PRAPS.G.TETRIS.Block(data.verb, data.preposition);
    this.justStopped = false;
    this.displayChoices = true;

    this.remove = false;

    this.render = function() {
        var index;
        if (this.displayChoices) {
            for (index=0; index < this.choices.length; index++) {
                this.choices[index].render();
            }
        }
        this.block.render();
    };

    this.update = function() {
        this.block.update();
        if (this.block.justReachedBottom) {
            this.justStopped = true;
        } else {
            this.justStopped = false;
        }
        if (this.wrongChoice) {
            this.choices[this.selectedChoice].wrong = true;
        }
    };

    this.success = function() {
        var index;
        for (index=0; index < this.choices.length; index++) {
            this.choices[index].remove = true;
        }
        this.block.remove = true;
    };

    this.fail = function () {
        this.choices.length = 0;
    }
};

PRAPS.G.TETRIS.Block = function(verb, preposition) {

    this.type = 'block';
    this.text = verb;
    this.preposition = preposition;
    this.width = 240;
    this.height = 50;
    this.speed = (Math.random() * 3) + 1;
    this.bottom = PRAPS.G.TETRIS.bottom;
    this.reachedBottom = false;
    this.justReachedBottom = false;

    this.x = Math.random() * (PRAPS.G.TETRIS.WIDTH - this.width);
    this.y = 0;

    this.remove = false;

    this.update = function() {

        //this.y -= this.speed;
        if (!this.reachedBottom) {
            this.y += this.speed;

            if (this.y + this.height >= this.bottom) {
                PRAPS.G.TETRIS.score.escaped += 1; // update score
                this.x = 0;
                this.width = PRAPS.G.TETRIS.WIDTH;
                this.speed = 0;
                this.reachedBottom = true;
                this.justReachedBottom = true;
                PRAPS.G.TETRIS.bottom = this.y;
                this.text += " " + this.preposition;
            }
        } else {
            this.justReachedBottom = false;
        }

    };

    this.render = function() {
        PRAPS.G.TETRIS.Draw.rect(this.x, this.y, this.width, this.height, '#660066');
        PRAPS.G.TETRIS.Draw.text(this.text, this.x + this.width/2, this.y + this.height/2, 24, 'white');
    };

};

PRAPS.G.TETRIS.Choice = function(text, correct) {

    this.type = 'choice';
    this.width = PRAPS.G.TETRIS.WIDTH/4;
    this.height = 80;
    this.text = text;
    this.correct = correct;
    this.wrong = false;

    this.x = 0;
    this.y = PRAPS.G.TETRIS.HEIGHT - 80;

    this.remove = false;

    this.render = function() {
        PRAPS.G.TETRIS.Draw.text(this.text, this.x + this.width/2, this.y + this.height/2, 24, 'white');
        if (this.wrong) {
            PRAPS.G.TETRIS.Draw.wrong(this.x, this.y, this.x+this.width, this.y+this.height);
            PRAPS.G.TETRIS.Draw.wrong(this.x+this.width, this.y, this.x, this.y+this.height);
        }
    };

};

PRAPS.G.TETRIS.ChoiceRect = function(color, x) {
    this.width = PRAPS.G.TETRIS.WIDTH/4;
    this.height = 80;
    this.x = x;
    this.y = PRAPS.G.TETRIS.HEIGHT - 80;
    this.color = color
    this.render = function() {
        PRAPS.G.TETRIS.Draw.rect(this.x, this.y, this.width, this.height, this.color);
    }
};

PRAPS.G.TETRIS.Particle = function(x, y, r, col) {

    this.x = x;
    this.y = y;
    this.r = r;
    this.col = col;

    // determines whether particle will
    // travel to the right of left
    // 50% chance of either happening
    this.dir = (Math.random() * 2 > 1) ? 1 : -1;

    // random values so particles do no
    // travel at the same speeds
    this.vx = ~~(Math.random() * 4) * this.dir;
    this.vy = ~~(Math.random() * 7);

    this.remove = false;

    this.update = function() {

        // update coordinates
        this.x += this.vx;
        //this.y -= this.vy;
        this.y -= this.vy;

        // increase velocity so particle
        // accelerates off screen
        this.vx *= 0.99;
        this.vy *= 0.99;

        // adding this negative amount to the
        // y velocity exerts an upward pull on
        // the particle, as if drawn to the
        // surface
        this.vy -= 0.25;

        // offscreen
        if (this.y < 0) {
            this.remove = true;
        }

    };


    this.render = function() {
        PRAPS.G.TETRIS.Draw.circle(this.x, this.y, this.r, this.col);
    };

};

//window.addEventListener('load', PRAPS.G.TETRIS.init, false);
//window.addEventListener('resize', PRAPS.G.TETRIS.resize, false);