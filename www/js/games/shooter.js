PRAPS.G.SHOOTER = {
    
    // set up some inital values
    WIDTH: 640,
    HEIGHT:  1136,
    scale:  1,
    // the position of the canvas
    // in relation to the screen
    offset: {top: 0, left: 0},
    // store all bubble, touches, particles etc
    entities: [],
    bullets: [],

    nextGroup: true,
    gameOver: false,
    nextBubble: 150,

    // we'll set the rest of these
    // in the init function
    RATIO:  null,
    currentWidth:  null,
    currentHeight:  null,
    canvas: null,
    ctx:  null,
    ua:  null,
    android: null,
    ios:  null,

    timer: null,

    requestAnimationFrame: window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame,

    cancelAnimationFrame: window.cancelAnimationFrame || window.mozCancelAnimationFrame,

    elements: {
        gameOverResult: function() {
            return $("#game-over-result");
        },
        resultScoreHolder: function() {
            return $("#game-over-result").find('.the-score');
        },
        maxPossibleScore: function() {
            return $("#game-over-result").find('.max-possible-score');
        },
        resultHeader: function() {
            return $("#game-over-result").find('.header');
        },
        resultMessage: function() {
            return $("#game-over-result").find('.message');
        },
        learnButton: function() {
            return $(".praps-learn-button");
        },
        exitButton: function() {
            return $(".exit-button");
        }
    },

    init: function() {
        // Silence please
        PRAPS.U.AudioComponent.stopMusic();

        // Hide iOS statusbar please
        if(window.StatusBar) {StatusBar.hide();}

        if(PRAPS.G.SHOOTER.timer) {
            cancelAnimationFrame(PRAPS.G.SHOOTER.timer);
        }

        // Silence please
        PRAPS.U.AudioComponent.stopMusic();

        // the proportion of width to height
        PRAPS.G.SHOOTER.RATIO = PRAPS.G.SHOOTER.WIDTH / PRAPS.G.SHOOTER.HEIGHT;
        // these will change when the screen is resize
        PRAPS.G.SHOOTER.currentWidth = PRAPS.G.SHOOTER.WIDTH;
        PRAPS.G.SHOOTER.currentHeight = PRAPS.G.SHOOTER.HEIGHT;

        //PRAPS.G.SHOOTER.bottom = PRAPS.G.SHOOTER.HEIGHT - 80;

        // this is our canvas element
        PRAPS.G.SHOOTER.canvas = document.getElementsByTagName('canvas')[0];
        // it's important to set this
        // otherwise the browser will
        // default to 320x200
        PRAPS.G.SHOOTER.canvas.width = PRAPS.G.SHOOTER.WIDTH;
        PRAPS.G.SHOOTER.canvas.height = PRAPS.G.SHOOTER.HEIGHT;
        // the canvas context allows us to
        // interact with the canvas api
        PRAPS.G.SHOOTER.ctx = PRAPS.G.SHOOTER.canvas.getContext('2d');
        // we need to sniff out android & ios
        // so we can hide the address bar in
        // our resize function
        PRAPS.G.SHOOTER.ua = navigator.userAgent.toLowerCase();
        PRAPS.G.SHOOTER.android = PRAPS.G.SHOOTER.ua.indexOf('android') > -1 ? true : false; // todo: simplify
        PRAPS.G.SHOOTER.ios = ( PRAPS.G.SHOOTER.ua.indexOf('iphone') > -1 || PRAPS.G.SHOOTER.ua.indexOf('ipad') > -1  ) ? true : false; // todo: simplify

        //exit button click handler
        PRAPS.G.BUBBLES.elements.exitButton().on('click', function() {
            cancelAnimationFrame(PRAPS.G.BUBBLES.timer);
            PRAPS.U.goToDashboard();
        });

        // listen for clicks
         window.addEventListener('click', function(e) {
         e.preventDefault();
         //PRAPS.G.SHOOTER.Input.set(e);
         }, false);

        // listen for touches
        window.addEventListener('touchstart', function(e) {
            e.preventDefault();
            // the event object has an array
            // called touches, we just want
            // the first touch
            PRAPS.G.SHOOTER.Input.set(e.touches[0]);
            PRAPS.G.SHOOTER.Input.startHold();
        }, false);
        window.addEventListener('touchmove', function(e) {
            // we're not interested in this
            // but prevent default behaviour
            // so the screen doesn't scroll
            // or zoom
            e.preventDefault();
            PRAPS.G.SHOOTER.Input.set(e);
            PRAPS.G.SHOOTER.Input.move();
        }, false);
        window.addEventListener('touchend', function(e) {
            // as above
            e.preventDefault();
            PRAPS.G.SHOOTER.Input.stopHold();

        }, false);

        window.addEventListener('mousedown', function(e) {
            e.preventDefault();
            // the event object has an array
            // called touches, we just want
            // the first touch
            PRAPS.G.SHOOTER.Input.set(e);
            PRAPS.G.SHOOTER.Input.startHold();
        }, false);
        window.addEventListener('mouseup', function(e) {
            // as above
            e.preventDefault();
            PRAPS.G.SHOOTER.Input.stopHold();

        }, false);
        window.addEventListener('mousemove', function(e) {
            // as above
            e.preventDefault();
            PRAPS.G.SHOOTER.Input.set(e);
            PRAPS.G.SHOOTER.Input.move();

        }, false);

        // we're ready to resize
        PRAPS.G.SHOOTER.resize();
        PRAPS.G.SHOOTER.reset();

        PRAPS.G.SHOOTER.loop();

    },

    reset: function() {
        PRAPS.G.SHOOTER.entities.length = 0;
        PRAPS.G.SHOOTER.Data.initialize();
        PRAPS.G.SHOOTER.Input.reset();
        PRAPS.G.SHOOTER.Score.reset();
        PRAPS.G.SHOOTER.Tank.reset();
        PRAPS.G.SHOOTER.BubbleTracker.reset();
        PRAPS.G.SHOOTER.Tracker.reset();
        PRAPS.G.SHOOTER.Sentence.reset();
        PRAPS.G.SHOOTER.Tracker.newSentence();
    },


    resize: function() {

        //PRAPS.G.SHOOTER.currentHeight = window.innerHeight;
        // resize the width in proportion
        // to the new height
        //PRAPS.G.SHOOTER.currentWidth = PRAPS.G.SHOOTER.currentHeight * PRAPS.G.SHOOTER.RATIO;

        PRAPS.G.SHOOTER.currentWidth = window.innerWidth;
        PRAPS.G.SHOOTER.currentHeight = PRAPS.G.SHOOTER.currentWidth / PRAPS.G.SHOOTER.RATIO;

        // this will create some extra space on the
        // page, allowing us to scroll pass
        // the address bar, and thus hide it.
        if (PRAPS.G.SHOOTER.android || PRAPS.G.SHOOTER.ios) {
            //document.body.style.height = (window.innerHeight + 50) + 'px';
        }

        // set the new canvas style width & height
        // note: our canvas is still 320x480 but
        // we're essentially scaling it with CSS
        PRAPS.G.SHOOTER.canvas.style.width = PRAPS.G.SHOOTER.currentWidth + 'px';
        PRAPS.G.SHOOTER.canvas.style.height = PRAPS.G.SHOOTER.currentHeight + 'px';

        // the amount by which the css resized canvas
        // is different to the actual (480x320) size.
        PRAPS.G.SHOOTER.scale = PRAPS.G.SHOOTER.currentWidth / PRAPS.G.SHOOTER.WIDTH;
        // position of canvas in relation to
        // the screen
        PRAPS.G.SHOOTER.offset.top = PRAPS.G.SHOOTER.canvas.offsetTop;
        PRAPS.G.SHOOTER.offset.left = PRAPS.G.SHOOTER.canvas.offsetLeft;

        // we use a timeout here as some mobile
        // browsers won't scroll if there is not
        // a small delay
        window.setTimeout(function() {
            window.scrollTo(0,1);
        }, 1);
    },

    // this is where all entities will be moved
    // and checked for collisions etc
    update: function() {
        var hit;
        //var checkCollision = false; // we only need to check for a collision
        // if the user tapped on this game tick

        PRAPS.G.SHOOTER.Tracker.update();

        // spawn a new instance of Touch
        // if the user has tapped the screen
        if (PRAPS.G.SHOOTER.Input.tapped) {
            // set tapped back to false
            // to avoid spawning a new touch
            // in the next cycle
            PRAPS.G.SHOOTER.Input.tapped = false;
        }

        //loop through and update all entities
        for (var i = 0; i < PRAPS.G.SHOOTER.entities.length; i++) {
            PRAPS.G.SHOOTER.entities[i].update();

            // delete from array if remove property flag is set to true
            if (PRAPS.G.SHOOTER.entities[i].remove) {
                //if a bubble is removed, set this in BubbleTracker
                if (PRAPS.G.SHOOTER.entities[i].type === 'bubble') {
                    PRAPS.G.SHOOTER.BubbleTracker.remove(PRAPS.G.SHOOTER.entities[i].position);
                    PRAPS.G.SHOOTER.Tracker.bubbleRemoved();
                }
                PRAPS.G.SHOOTER.entities.splice(i, 1);
            }
        }

        //loop through bullets and check for collision with bubbles
        for (i = 0; i < PRAPS.G.SHOOTER.bullets.length; i++) {
            PRAPS.G.SHOOTER.bullets[i].update();

            for (var j = 0; j <PRAPS.G.SHOOTER.entities.length; j++) {
                if (PRAPS.G.SHOOTER.entities[j].type === 'bubble') {
                    //check for collision between bullet and bubble
                    hit = PRAPS.G.SHOOTER.collides(PRAPS.G.SHOOTER.bullets[i], PRAPS.G.SHOOTER.entities[j]);
                    //if there is a collision create particles
                    if (hit) {
                        for (var n = 0; n < 5; n++) {
                            PRAPS.G.SHOOTER.entities.push(new PRAPS.G.SHOOTER.Particle(
                                PRAPS.G.SHOOTER.entities[j].x,
                                PRAPS.G.SHOOTER.entities[j].y,
                                2,
                                // random opacity to spice it up a bit
                                'rgba(255,255,255,'+Math.random()*1+')'
                            ));
                        }
                        //test for correct answer
                        PRAPS.G.SHOOTER.Sentence.test(PRAPS.G.SHOOTER.entities[j].preposition);
                        //remove the bullet and bubble
                        PRAPS.G.SHOOTER.bullets[i].remove = true;
                        PRAPS.G.SHOOTER.entities[j].remove = true;
                    }
                }
            }

            //remove bullets
            if (PRAPS.G.SHOOTER.bullets[i].remove) {
                PRAPS.G.SHOOTER.bullets.splice(i, 1);
            }
        }

        PRAPS.G.SHOOTER.Sentence.update();
        PRAPS.G.SHOOTER.Tank.update();
        PRAPS.G.SHOOTER.Score.update();

    },


    // this is where we draw all the entities
    render: function() {
        //PRAPS.G.SHOOTER.Draw.rect(0, 0, PRAPS.G.SHOOTER.WIDTH, PRAPS.G.SHOOTER.HEIGHT, '#6699FF');
        PRAPS.G.SHOOTER.Draw.background();

        // cycle through all entities and render to canvas
        for (var i = 0; i < PRAPS.G.SHOOTER.entities.length; i++) {
            PRAPS.G.SHOOTER.entities[i].render();
        }

        for (i = 0; i < PRAPS.G.SHOOTER.bullets.length; i++) {
            PRAPS.G.SHOOTER.bullets[i].render();
        }

        PRAPS.G.SHOOTER.Tracker.render();
        PRAPS.G.SHOOTER.Score.render();
        PRAPS.G.SHOOTER.Sentence.render();
        PRAPS.G.SHOOTER.Tank.render();
    },


    // the actual loop
    // requests animation frame
    // then proceeds to update
    // and render
    loop: function() {
        PRAPS.G.SHOOTER.timer = requestAnimationFrame(PRAPS.G.SHOOTER.loop);
        PRAPS.G.SHOOTER.update();
        PRAPS.G.SHOOTER.render();
    }


};

// checks if two entties are touching
PRAPS.G.SHOOTER.collides = function(a, b) {

    var distance_squared = ( ((a.x - b.x) * (a.x - b.x)) +
        ((a.y - b.y) * (a.y - b.y)));

    var radii_squared = (a.r + b.r) * (a.r + b.r);

    if (distance_squared < radii_squared) {
        return true;
    } else {
        return false;
    }
};



// abstracts various canvas operations into
// standalone functions
PRAPS.G.SHOOTER.Draw = {

    clear: function() {
        PRAPS.G.SHOOTER.ctx.clearRect(0, 0, PRAPS.G.SHOOTER.WIDTH, PRAPS.G.SHOOTER.HEIGHT);
    },


    rect: function(x, y, w, h, col) {
        PRAPS.G.SHOOTER.ctx.fillStyle = col;
        PRAPS.G.SHOOTER.ctx.fillRect(x, y, w, h);
    },

    circle: function(x, y, r, col) {
        PRAPS.G.SHOOTER.ctx.fillStyle = col;
        PRAPS.G.SHOOTER.ctx.beginPath();
        PRAPS.G.SHOOTER.ctx.arc(x + 5, y + 5, r, 0,  Math.PI * 2, true);
        PRAPS.G.SHOOTER.ctx.closePath();
        PRAPS.G.SHOOTER.ctx.fill();
    },


    text: function(string, x, y, size, col, align) {
        //PRAPS.G.SHOOTER.ctx.font = 'bold '+size+'px Arial';
        PRAPS.G.SHOOTER.ctx.font = '700 ' + size + 'px "NotoSans"';
        PRAPS.G.SHOOTER.ctx.fillStyle = col;
        PRAPS.G.SHOOTER.ctx.textAlign=align;
        PRAPS.G.SHOOTER.ctx.fillText(string, x, y);
    },

    line: function(x, y, w, h) {
        PRAPS.G.SHOOTER.ctx.beginPath();
        PRAPS.G.SHOOTER.ctx.moveTo(x, y);
        PRAPS.G.SHOOTER.ctx.lineTo(w, h);
        PRAPS.G.SHOOTER.ctx.stroke();
    },
    background: function(){
        PRAPS.G.SHOOTER.ctx.drawImage(PRAPS.V.appImages.bgShooter, 0, 0);
    },

    bullet: function(x, y) {
        PRAPS.G.SHOOTER.ctx.drawImage(PRAPS.V.appImages.bullet, x, y);
    },

    ufo: function(x, y, v) {
        PRAPS.G.SHOOTER.ctx.drawImage(PRAPS.V.appImages["ufo" + v], x, y);
    },
    ship: function(x, y) {
        PRAPS.G.SHOOTER.ctx.drawImage(PRAPS.V.appImages.ship, x, y);
    }
};

PRAPS.G.SHOOTER.Input = {

    x: 0,
    y: 0,
    tapped :false,

    set: function(data) {
        this.x = (data.pageX - PRAPS.G.SHOOTER.offset.left) / PRAPS.G.SHOOTER.scale;
        this.y = (data.pageY - PRAPS.G.SHOOTER.offset.top) / PRAPS.G.SHOOTER.scale;
        this.tapped = true;

    },

    startHold: function() {
        PRAPS.G.SHOOTER.Tank.start(this.x, this.y);
    },

    stopHold: function() {
        PRAPS.G.SHOOTER.Tank.stop();
    },

    move: function() {
        PRAPS.G.SHOOTER.Tank.move(this.x, this.y);
    },

    reset: function() {
        this.x = 0;
        this.y = 0;
        this.tapped = false;
    }

};

PRAPS.G.SHOOTER.Tracker = {
    gameOver: false,
    calledGameOver: false,
    numSentences: 10,
    ruleId: null,
    example: null,
    preposition: null,
    prepositionsList: [],
    prepositionsIndex: 0,
    rules: [],
    rulesIndex: 0,
    selectionsLeft: 0,
    nextBubble: 40,
    bubbleCounter: 0,
    correctList: [],
    wrongList: [],
    y: PRAPS.G.SHOOTER.HEIGHT-30,

    newSentence: function() {
        if (this.rulesIndex < this.rules.length) {
        //if (this.rulesIndex < 2) { <-- For debugging
            var tempRule = this.rules[this.rulesIndex];
        
            this.ruleId = tempRule.id;
            this.example = tempRule.example.replace(/\[[^\]]+\]/g, '___');
            this.preposition = tempRule.preposition;
            
            var tempPrepositionsList = tempRule.prepositionsList.slice();
            //randomize order of prepositions
            for (var i = tempPrepositionsList.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = tempPrepositionsList[i];
                tempPrepositionsList[i] = tempPrepositionsList[j];
                tempPrepositionsList[j] = temp;
            }
            this.prepositionsList = tempPrepositionsList;
            

            this.rulesIndex++;
            this.prepositionsIndex = 0;
            this.selectionsLeft = 5;

            PRAPS.G.SHOOTER.Sentence.change(this.example, this.preposition);  
        } else {
            this.renderGameOver();
        }
    },

    correctSelection: function() {
        //if 0 or 1 miestakes are made add to correctList
        if (this.selectionsLeft >= 4) {
            this.correctList.push(this.ruleId);
        }
        PRAPS.G.SHOOTER.Score.addRight();
        this.newSentence();
    },

    wrongSelection: function() {
        this.selectionsLeft--;
        PRAPS.G.SHOOTER.Score.addWrong();
        if (this.selectionsLeft <= 0) {
            this.wrongList.push(this.ruleId);
            this.newSentence();
        }
    },

    bubbleRemoved: function() {
        this.bubbleCounter--;
    },

    isGameOver: function() {
        return this.gameOver;
    },

    update: function() {
        this.nextBubble--;
        // if the time in between bubbles is less than zero
        if (this.nextBubble < 0) {
            //check that bubble index is less than length of array
            if (this.prepositionsIndex < this.prepositionsList.length) {
                // put a new instance of bubble into our entities array
                if (!this.gameOver) {
                    var idx = Math.ceil(Math.random() * 4);
                    //console.log(idx);
                    
                    position = PRAPS.G.SHOOTER.BubbleTracker.findFree();
                    xValue = PRAPS.G.SHOOTER.BubbleTracker.getX(position);
                    if (position >= 0) {
                        PRAPS.G.SHOOTER.entities.push(new PRAPS.G.SHOOTER.Bubble(
                            this.prepositionsList[this.prepositionsIndex], 
                            xValue, position, idx));
                        PRAPS.G.SHOOTER.BubbleTracker.add(position);
                        this.prepositionsIndex++;
                        this.bubbleCounter++;
                    }
                }
                // reset the counter with a random value
                this.nextBubble = ( Math.random() * 40 ) + 40;  
             } 
            

            if (!this.gameOver) {
                //check if all bubbles have been removed
                if (this.bubbleCounter <= 0) {
                    //if run out of prepositions, then create a new sentence
                    if (this.ruleId !== null) {
                        this.wrongList.push(this.ruleId);
                    }
                    this.newSentence();
                } 
            }
            
        }
            
    },

    render: function() {
        PRAPS.G.SHOOTER.Draw.text((this.numSentences - this.rulesIndex + 1), PRAPS.G.SHOOTER.WIDTH - 15, PRAPS.G.SHOOTER.HEIGHT - 15, 70, '#fff', 'right');
        //PRAPS.G.SHOOTER.Draw.text("Selections left: "+this.selectionsLeft, PRAPS.G.SHOOTER.WIDTH/2+30, this.y, 30, 'black', 'left');
        //PRAPS.G.SHOOTER.Draw.text("Score: "+PRAPS.G.SHOOTER.Score.getScore(), 20, this.y-50, 30, 'black', 'left');//
    },

    renderGameOver: function() {
        this.gameOver = true;

        if (!this.calledGameOver) {
            this.calledGameOver = true;

            PRAPS.G.SHOOTER.elements.resultHeader().text("Game Over");
            PRAPS.G.SHOOTER.elements.resultMessage().text("Here's how you did. Keep on learning!");
            PRAPS.G.SHOOTER.elements.resultScoreHolder().text(PRAPS.G.SHOOTER.Score.getScore());
            PRAPS.G.SHOOTER.elements.maxPossibleScore().text(PRAPS.G.SHOOTER.Score.maxScore);
            PRAPS.U.StorageComponent.updateAppStatistics(
                {guessedRightAway: PRAPS.G.SHOOTER.Tracker.correctList, 
                    failedRightAway: PRAPS.G.SHOOTER.Tracker.wrongList});
            PRAPS.U.StorageComponent.updateScoresSums(
                PRAPS.G.SHOOTER.Score.getScore(), PRAPS.G.SHOOTER.Score.maxScore-1);
            PRAPS.G.BUBBLES.elements.exitButton().hide();

            cancelAnimationFrame(PRAPS.G.SHOOTER.timer);
            if(window.analytics) { analytics.trackEvent('Results', 'Shooter Result', 'Score', PRAPS.G.SHOOTER.Score.getScore() / PRAPS.G.SHOOTER.Score.maxScore);}

            PRAPS.G.SHOOTER.elements.gameOverResult()
                .css({"display": "block"})
                .addClass('animated bounceInDown')
                .on('click', '.praps-ok-button', function() {
                    //cancelAnimationFrame(PRAPS.G.SHOOTER.timer);
                    if(window.analytics) { analytics.trackEvent('Learning', 'Shooter Learning', 'No'); }
                    PRAPS.U.goToDashboard();
                    if(PRAPS.V.isMusic){
                        PRAPS.U.AudioComponent.playMusic();
                    }                })
                .on('click', '.praps-learn-button', function(e) {
                    e.preventDefault();
                    if(window.analytics) { analytics.trackEvent('Learning', 'Shooter Learning', 'Yes', PRAPS.V.currentFlashCardsArray.length); }
                    PRAPS.U.goToFlashCards();
                });
        } 
    },

    reset: function() {
        this.gameOver = false;
        this.calledGameOver = false;
        this.numSentences = 10;
        this.ruleId = null;
        this.example = null;
        this.preposition = null;
        this.prepositionsList.length = 0;
        this.prepositionsIndex = 0;
        this.rules.length = 0;
        this.rulesIndex = 0;
        this.selectionsLeft = 0;
        this.nextBubble = 40;
        this.bubbleCounter = 0;
        this.correctList.length = 0;
        this.wrongList.length = 0;
        this.y = PRAPS.G.SHOOTER.HEIGHT-30;

        this.rules = PRAPS.G.SHOOTER.Data.getNRules(this.numSentences);
    }
};

PRAPS.G.SHOOTER.BubbleTracker = {
    exists: [false, false, false, false],

    xValues: [PRAPS.G.SHOOTER.WIDTH/8, 3*PRAPS.G.SHOOTER.WIDTH/8,
              5*PRAPS.G.SHOOTER.WIDTH/8, 7*PRAPS.G.SHOOTER.WIDTH/8],       

    findFree: function() {
        var available = [];
        var index;
        for (var i = 0; i < this.exists.length; i++) {
            if (!this.exists[i]) {
                available.push(i);
            }
        }

        if (available.length > 0) {
            return available[Math.floor(Math.random() * available.length)];
        } else {
            return -1;
        }
    },

    getX: function(position) {
        if (position >= 0 && position <= 3) {
            return this.xValues[position];
        } else {
            return 0;
        }
    },

    add: function(position) {
        this.exists[position] = true;
    },

    remove: function(position) {
        this.exists[position] = false;
    },

    reset: function() {
        this.exists.length = 0;
        this.xValues.length = 0;

        this.exists = [false, false, false, false];
        this.xValues = [PRAPS.G.SHOOTER.WIDTH/8, 3*PRAPS.G.SHOOTER.WIDTH/8,
                        5*PRAPS.G.SHOOTER.WIDTH/8, 7*PRAPS.G.SHOOTER.WIDTH/8];
    }
};

PRAPS.G.SHOOTER.Bubble = function(preposition, x, position, idx) {
    this.type = 'bubble';
    this.r = 55;
    this.speed = (Math.random() * 3) + 1;

    this.x = x;
    this.y = -50;
    this.position = position;
    this.preposition = preposition;
    this.color = 'rgba(255, 255, 255, .2)';
    this.remove = false;

    this.update = function() {
        this.y += this.speed;
        if (this.y >= PRAPS.G.SHOOTER.HEIGHT+this.r) {
            this.remove = true;
        }
    };

    this.render = function() {
        PRAPS.G.SHOOTER.Draw.ufo(this.x - 60, this.y - 60, idx);
        //PRAPS.G.SHOOTER.Draw.circle(this.x, this.y, this.r, this.color);
        PRAPS.G.SHOOTER.Draw.text(this.preposition, this.x, this.y+15, 30, '#fff', 'center');
    };

};

PRAPS.G.SHOOTER.Bullet = function(x, y) {
    this.type = 'bullet';
    this.speed = 8;
    this.r = 10;
    this.x = x;
    this.y = y;
    this.color = '#4C4C4C';
    this.remove = false;

    this.update = function() {
        this.y -= this.speed;
        if (this.y <= -this.r) {
            this.remove = true;
        }
    };

    this.render = function() {
        PRAPS.G.SHOOTER.Draw.bullet(this.x, this.y - 10);
        //PRAPS.G.SHOOTER.Draw.circle(this.x, this.y, this.r, this.color);
    };

};

PRAPS.G.SHOOTER.Particle = function(x, y, r, col) {

    this.x = x;
    this.y = y;
    this.r = r;
    this.col = col;

    // determines whether particle will
    // travel to the right of left
    // 50% chance of either happening
    this.dir = (Math.random() * 2 > 1) ? 1 : -1;

    // random values so particles do no
    // travel at the same speeds
    this.vx = ~~(Math.random() * 4) * this.dir;
    this.vy = ~~(Math.random() * 7);

    this.remove = false;

    this.update = function() {

        // update coordinates
        this.x += this.vx;
        //this.y -= this.vy;
        this.y -= this.vy;

        // increase velocity so particle
        // accelerates off screen
        this.vx *= 0.99;
        this.vy *= 0.99;

        // adding this negative amount to the
        // y velocity exerts an upward pull on
        // the particle, as if drawn to the
        // surface
        this.vy -= 0.25;

        // offscreen
        if (this.y > PRAPS.G.SHOOTER.HEIGHT) {
            this.remove = true;
        }

    };


    this.render = function() {
        PRAPS.G.SHOOTER.Draw.circle(this.x, this.y, this.r, this.col);
    };

};



PRAPS.G.SHOOTER.Tank = {
    type: 'tank',
    x: PRAPS.G.SHOOTER.WIDTH/2-100,
    y: PRAPS.G.SHOOTER.HEIGHT - 100,
    w: 50,
    h: 100,
    xOffset: 29,
    yOffset: 35,
    speed: 0,
    color: 'green',
    hold: false,
    inputX: null,
    inputY: null,
    timer: 0,
    shoot: false,

    start: function(x, y) {
        this.inputX = x;
        this.inputY = y;

        if (y >= this.y - this.yOffset) {
            if (x > (this.x-50) && x < (this.x+this.w+50)) {
                this.hold = true;
            }
        }

        this.timer = 20;
    },

    stop: function() {
        if (this.timer > 0) {
            this.shootBullet();
            this.timer = 0;
        }

        this.hold = false;
    },

    move: function(x, y) {
        this.inputX = x;
        this.inputY = y;

        if (x >= (this.x+this.w/2)) {
            this.speed = 6;
        } else {
            this.speed = -6;
        }
    },

    shootBullet: function() {
        /*
        if (this.inputY >= this.y) {
            if (this.inputX > this.x && this.inputX < this.x+this.w) {
                this.shoot = true;
            }
        }
        */
        if (this.inputY > PRAPS.G.SHOOTER.Sentence.sentenceHeight()) {
            this.shoot = true;
        }
    },

    update: function() {
        this.timer--;

        if (this.shoot) {
            //Create new bullet
            PRAPS.G.SHOOTER.bullets.push(new PRAPS.G.SHOOTER.Bullet(this.x+this.w/2, this.y));
            this.shoot = false;
        }

        if (this.hold) {
            if (this.x <= 0 && this.speed < 0) {
                this.x += 0;
            } else if (this.x <= 0 && this.speed > 0) {
                this.x += this.speed;
            } else if (this.x >= (PRAPS.G.SHOOTER.WIDTH-this.w) && this.speed > 0) {
                this.x += 0;
            } else if (this.x >= (PRAPS.G.SHOOTER.WIDTH-this.w) && this.speed > 0) {
                this.x += this.speed;
            } else if (this.x >= (this.inputX-this.w) && this.speed > 0) {
                this.x += 0;
            } else if (this.x <= this.inputX && this.speed < 0) {
                this.x += 0;
            } else {
                this.x += this.speed;
            }
        }
    },

    reset: function() {
        this.x = PRAPS.G.SHOOTER.WIDTH/2 - 40;
        this.y = PRAPS.G.SHOOTER.HEIGHT - 100;
        this.w = 80;
        this.h = 100;
        this.speed = 0;
        this.moveTank = false;
        this.color = 'rgba(255, 255, 255, .2)';
        this.hold = false;
        this.inputX = null;
        this.inputY = null;
        this.timer = 0;
        this.shoot = false;
    },

    render: function() {
        PRAPS.G.SHOOTER.Draw.ship(this.x - this.xOffset, this.y - this.yOffset);
    }
};

PRAPS.G.SHOOTER.Sentence = {
    type: 'sentence',

    x: 0,
    y: 100,
    w: PRAPS.G.SHOOTER.WIDTH,
    h: 100,

    preposition: null,
    text: "",
    textPrev: "",
    textX: 0,
    textY: 150,
    textXPrev: null,
    isSliding: false,

    change: function(s, p) {
        this.textPrev = this.text;
        this.textXPrev = this.textX;
        this.isSliding = true;
        this.textX = 1.5*PRAPS.G.SHOOTER.WIDTH;

        this.text = s;
        this.preposition = p;
    },

    slide: function() {
        this.textX -= 10;
        this.textXPrev -= 10;
        if (this.textX === PRAPS.G.SHOOTER.WIDTH/2) {
            this.isSliding = false;
            this.textPrev = "";
        }
    },

    test: function(preposition) {
        if (this.preposition === preposition) {
            PRAPS.G.SHOOTER.Tracker.correctSelection(preposition);
            return true;
        } else {
            PRAPS.G.SHOOTER.Tracker.wrongSelection(preposition);
            return false;
        }
    },

    sentenceHeight: function() {
        return this.h;
    },

    update: function() {
        if (this.isSliding) {
            this.slide();
        }
    },

    render: function() {
        PRAPS.G.SHOOTER.Draw.rect(this.x, this.y, this.w, this.h, 'rgba(255, 255, 255, .15)');
        PRAPS.G.SHOOTER.Draw.text(this.text, this.textX, this.textY, 23, '#fff', 'center');
        PRAPS.G.SHOOTER.Draw.text(this.textPrev, this.textXPrev, this.textY, 23, '#fff', 'center');
        //PRAPS.G.SHOOTER.Draw.line(this.x, this.y+this.h, this.w, this.y+this.h);
    },

    reset: function() {
        this.x = 0;
        this.y = 0;
        this.w = PRAPS.G.SHOOTER.WIDTH;
        this.h = 65;
        this.xOffset = 29;
        this.yOffset = 35;
        this.text = "";
        this.textPrev = "";
        this.preposition = null;
        this.textPrev = "";
        this.textX = 0;
        this.textY = 40;
        this.textXPrev = null;
        this.isSliding = false;
    }

};

PRAPS.G.SHOOTER.Score = {
    x: 0,
    y: 0,
    w: PRAPS.G.SHOOTER.WIDTH,
    h: 100,

    right: 0,
    wrong: 0,

    rightColor: 'white',
    wrongColor: 'white',

    rightCounter: 0,
    wrongCounter: 0,

    rightScore: 100,
    wrongScore: 10,

    maxScore: 1000,

    addRight: function() {
        this.right++;
        this.rightColor = 'green';
        this.rightCounter = 50;
    },

    addWrong: function() {
        this.wrong++;
        this.wrongColor = 'red';
        this.wrongCounter = 50;
    },

    update: function() {
        this.rightCounter--;
        this.wrongCounter--;

        if (this.rightCounter <= 0) {
            this.rightColor = 'white';
        }

        if (this.wrongCounter <= 0) {
            this.wrongColor = 'white';
        }
    },

    getScore: function() {
        var score = this.right*this.rightScore-this.wrong*this.wrongScore;
        if (score < 0) {
            score = 0;
        }
        return score;
    },

    reset: function() {
        this.right = 0;
        this.wrong = 0;
        this.rightCounter = 0;
        this.wrongCounter = 0;
    },

    render: function() {
        //PRAPS.G.SHOOTER.Draw.rect(this.x, this.y, this.w/2, this.h, this.rightColor);
        //PRAPS.G.SHOOTER.Draw.rect(this.x+this.w/2, this.y, this.w/2, this.h, this.wrongColor);
        //PRAPS.G.SHOOTER.Draw.text("Richtig: "+this.right, 10, PRAPS.G.SHOOTER.HEIGHT - 15, 70, '#78ba52', 'left');
        PRAPS.G.SHOOTER.Draw.text(this.right, 15, PRAPS.G.SHOOTER.HEIGHT - 15, 50, '#78ba52', 'left');
        //PRAPS.G.SHOOTER.Draw.text("Falsch: "+this.wrong, 10, PRAPS.G.SHOOTER.HEIGHT - 15, 70, '#fc7360', 'left');
        PRAPS.G.SHOOTER.Draw.text(this.wrong, 15, PRAPS.G.SHOOTER.HEIGHT - 65, 50, '#fc7360', 'left');
        //PRAPS.G.SHOOTER.Draw.line(this.w/2, this.y, this.w/2, this.h);
        //PRAPS.G.SHOOTER.Draw.line(this.x, this.h, this.w, this.h);
    }

};

PRAPS.G.SHOOTER.Data = {
    clonedArray: [],

    initialize: function() {
        this.clonedArray.length = 0;
        this.clonedArray = PRAPS.V.rawRulesDeck.slice();
    },

    getNRules: function(n) {
        var rulesList = [];
        var rulesUsed = {};
        var tempRule;
        var ruleUsed;

        if (this.clonedArray.length === 0) {
            this.initialize();
        }

        for (var i = 0; i < n; i++) {
            ruleUsed = true;
            while (ruleUsed) {
                tempRule = this.clonedArray[Math.floor(Math.random() * this.clonedArray.length)];
                if (!(tempRule.id in rulesUsed)) {
                    ruleUsed = false;
                }
            }
            rulesList.push(tempRule);
            rulesUsed[tempRule.id] = true;
        }

        return rulesList;

    }
};